VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSFTP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private sftp As New ChilkatSFtp, cSFTPHost As String, cSFTPUser As String, cSFTPPwd As String, cSFTPRemoteDir As String, cFTPError As String

Public Function Init(cHost As String, cUser As String, cPwd As String, cRemoteDir As String)
    Dim nResult As Long, lOK As Boolean
    lOK = True
    nResult = sftp.UnlockComponent("OAKSFT.CB1022020_ddzCMuHRne55")
    If (nResult <> 1) Then
        lOK = False
    End If
    If lOK Then
        cSFTPHost = cHost
        cSFTPUser = cUser
        cSFTPPwd = cPwd
        cSFTPRemoteDir = cRemoteDir
    End If
    Init = lOK
    
End Function

Public Function SFTPUpload(cSource As String, cDestination As String)
    Dim lOK As Boolean, nResult As Long, cHandle As String, cTwoFishKey As String
    Dim Key As New ChilkatSshKey, cPrivateKey As String, cTwoFish As New clsTwofish, bdKeyFileBytes As New ChilkatBinData, aKeyFileBytes() As Byte
    
    cTwoFishKey = "oAkSh0pExport1"
    lOK = True
    '  Upload a file.
    '  Set some timeouts, in milliseconds:
    sftp.ConnectTimeoutMs = 5000
    sftp.IdleTimeoutMs = 15000
    
    '  Connect to the SSH server.
    '  The standard SSH port = 22
    '  The hostname may be a hostname or IP address.
    nResult = sftp.Connect(cSFTPHost, 22)
    If (nResult <> 1) Then
        lOK = False
    End If

    'If lOK Then
    '    '  Authenticate with the SSH server.  Chilkat SFTP supports
    '    '  both password-based authenication as well as public-key
    '    '  authentication.  This example uses password authenication.
    '    nResult = sftp.AuthenticatePw(cSFTPUser, cSFTPPwd)
    '    If (nResult <> 1) Then
    '        lOK = False
    '    End If
    'End If
    'If lOK Then
    '    '  After authenticating, the SFTP subsystem must be initialized:
    '    nResult = sftp.InitializeSftp()
    '    If (nResult <> 1) Then
    '        lOK = False
    '    End If
    'End If
    
    If lOK Then
        ' use SSH key authentication
        ' Load a PEM file into a string variable:
        ' (This does not load the PEM file into the key.  The LoadText
        ' method is a convenience method for loading the full contents of ANY text
        ' file into a string variable.)
        ' get encrypted file contents
        bdKeyFileBytes.LoadFile (App.Path + "\oak-export-upload-files.oak")
        aKeyFileBytes() = bdKeyFileBytes.GetBinary
        ' decrypt contents
        Call cTwoFish.DecryptByte(aKeyFileBytes, cTwoFishKey)
        ' convert to a string
        cPrivateKey = StrConv(aKeyFileBytes(), vbUnicode)
        'MsgBox Len(cPrivateKey)
        'MsgBox bdKeyFileBytes.numBytes
        'nResult = key.LastMethodSuccess
        If Len(cPrivateKey) < 1600 Then
            lOK = False
            cFTPError = "Get Key error"
            Call Add2Log("    error ")
        End If
    End If
    
    If lOK Then
        ' Load a private key from a PEM string:
        ' (Private keys may be loaded from OpenSSH and Putty formats.
        ' Both encrypted and unencrypted private key file formats
        ' are supported.  This example loads an unencrypted private
        ' key in OpenSSH format.
        nResult = Key.FromOpenSshPrivateKey(cPrivateKey)
        If (nResult <> 1) Then
            lOK = False
            cFTPError = "Load key failed" + Str(nResult) + "-" + Key.LastErrorText
            Call Add2Log("    key:load failed " + Str(nResult))
        End If
    End If
    
    If lOK Then
        ' Authenticate with the SSH server.  Chilkat SFTP supports
        ' both password-based authenication as well as public-key
        ' authentication.
        nResult = sftp.AuthenticatePk(cSFTPUser, Key)
        If (nResult <> 1) Then
            lOK = False
            cFTPError = "Authentication failed" + Str(nResult) + "-" + Key.LastErrorText
            'MsgBox cFTPError
            Call Add2Log("    key:authentication failed " + Str(nResult))
        End If
    End If
    
    If lOK Then
        nResult = sftp.InitializeSftp()
        If (nResult <> 1) Then
            lOK = False
            cFTPError = "sftp initilization failed" + Str(nResult) + "-" + sftp.LastErrorText
            Call Add2Log("    sftp:initialization failed " + Str(nResult))
        End If
    End If

    If lOK Then
        '  Open a file for writing on the SSH server.
        '  If the file already exists, it is overwritten.
        '  (Specify "createNew" instead of "createTruncate" to
        '  prevent overwriting existing files.)
        Call Add2Log("upload file: " + cSource + " to " + cSFTPRemoteDir + cDestination, True)
        cHandle = sftp.OpenFile(cSFTPRemoteDir + cDestination, "writeOnly", "createTruncate")
        If (cHandle = vbNullString) Then
            lOK = False
            'MsgBox sftp.LastErrorText
        End If
    End If
    
    If lOK Then
        '  Upload from the local file to the SSH server.
        nResult = sftp.UploadFile(cHandle, cSource)
        If (nResult <> 1) Then
            lOK = False
        End If
    End If
    
    If (cHandle <> vbNullString) Then
        '  Close the file.
        nResult = sftp.CloseHandle(cHandle)
    End If
    SFTPUpload = lOK

End Function
Private Sub Add2Log(cAction As String, Optional lAddTime As Boolean)
    Dim nLogFile As Integer
    nLogFile = FreeFile
    Open App.Path + "\ShopExport.log" For Append As #nLogFile
    Print #nLogFile, cAction + IIf(lAddTime, " - " + Format(Now, "DD/MM/YYYY HH:MM:SS"), "")
    Close #nLogFile
End Sub
