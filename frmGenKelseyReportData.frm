VERSION 5.00
Begin VB.Form frmGenKelseyReportData 
   Caption         =   "Oak Report Viewer Data Generator"
   ClientHeight    =   3180
   ClientLeft      =   60
   ClientTop       =   360
   ClientWidth     =   4680
   Icon            =   "frmGenKelseyReportData.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3180
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   1095
      Left            =   120
      TabIndex        =   0
      Top             =   420
      Width           =   4215
   End
End
Attribute VB_Name = "frmGenKelseyReportData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim aDbfs() As String, cb As Long, data As New Collection, nFile As Integer, nLogFile As Integer, sCnn As cConnection, sTemp As cConnection
Dim rs As cRecordset, rsTemp As cRecordset, cSql As String, i As Long, cInsertSQL As String, cUpdateSQL As String, cFirstIssue As String
Dim aIssue(50, 6) As String, nIssuesPerYear As Integer, cFTPUserName As String
Dim cS3BucketName As String, cS3AccessKey As String, cS3SecretKey As String
Dim lAllowNegativeValueMOLines As Boolean
Private cLastMailOrderID As String, cLastNewSubSourceIDList As String, cLastNewSubDate As String

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
   "GetPrivateProfileStringA" (ByVal lpApplicationName _
   As String, ByVal lpKeyName As Any, ByVal lpDefault _
   As String, ByVal lpReturnString As String, ByVal _
   nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
   "WritePrivateProfileStringA" (ByVal lpApplicationName _
   As String, ByVal lpKeyName As Any, ByVal lpString As Any, _
   ByVal lpFileName As String) As Long

Private Function sGetINI(sINIFile As String, sSection As String, sKey As String, sDefault As String) As String
    Dim sTemp As String * 256
    Dim nLength As Integer
    sTemp = Space(256)
    nLength = GetPrivateProfileString(sSection, sKey, sDefault, sTemp, 255, sINIFile)
    sGetINI = Left(sTemp, nLength)
End Function

Private Sub WriteINI(sINIFile As String, sSection As String, sKey As String, sValue As String)
    Dim n As Integer
    Dim sTemp As String
    sTemp = sValue
    'replace any CR/LF characters with spaces
    For n = 1 To Len(sValue)
        If Mid(sValue, n, 1) = vbCr Or Mid(sValue, n, 1) = vbLf Then Mid(sValue, n, 1) = " "
    Next n
    n = WritePrivateProfileString(sSection, sKey, sTemp, sINIFile)
End Sub

' clear old mailing batch data
'-- 003797 = last mailing in June 2019
'delete from active_subs where as_batch_id>'003797';
'delete from future_lapsed where fl_batch_id>'003797';
'delete from mailed_value where mv_batch_id>'003797';
'delete from new_subs_response where nsr_batch_id>'003797';


Private Sub Form_Load()
    Dim cPayMethod As String, cIssue As String, nCount As Long, nValue As Double, cPub As String, nOrder As Integer, j As Integer, nIssues As Integer, nRenewalFile As Integer
    Dim nNewCount As Long, nNewValue As Double, nRenCount As Long, nRenValue As Double, cCountry As String, cAgents As String, cMonth As String, nType As Integer
    Dim cAgent As String, cSubType As String, cCountryDesc As String, aRenewal(11), cSource As String, cRegion As String, cFirstDD As String, cIni As New clsIni
    Dim cMailDate As String, lInclude As Boolean, lActive As Boolean, cLatestIssue As String, cFutureIssue As String, cChannel As String, nSource As Long, cNewRenewed As String
    Dim clsLogAutoProcess As New clsLogAutoProcess, cTwoFishKey As String, cTwoFish As New clsTwofish    ' clsLogAutoProcess is instance of the log class
    Dim cLatestNewSubSourceID As String
    
    Call FormGradient(Me, RGB(210, 230, 255), 1, 64)
    Show
    Label1 = "Starting...."
    Label1.Refresh
    lAllowNegativeValueMOLines = False
    
    cS3BucketName = "oak-app-data-upload"
    If Dir(App.Path + "\oakreports.ini") <> "" Then
        cTwoFishKey = "oAkSh0pExport1"
        If Not IsBlank(cIni.sGetINI(App.Path + "\oakreports.ini", "OakReports", "LastMailOrderID", "")) Then
            cFTPUserName = cTwoFish.DecryptString(Trim(cIni.sGetINI(App.Path + "\OakReports.ini", "OakReports", "FTPUsername", "")), cTwoFishKey, True)
        End If
        cS3AccessKey = cTwoFish.DecryptString(Trim(cIni.sGetINI(App.Path + "\oakreports.ini", "OakReports", "S3AccessKey", "")), cTwoFishKey, True)
        cS3SecretKey = cTwoFish.DecryptString(Trim(cIni.sGetINI(App.Path + "\oakreports.ini", "OakReports", "S3SecretKey", "")), cTwoFishKey, True)

        lAllowNegativeValueMOLines = Left(cIni.sGetINI(App.Path + "\oakreports.ini", "OakReports", "AllowNegativeValueMOLines", ""), 1) = "Y"
    End If
    
    If Len(Command) > 0 And UCase(Left(Command, 11)) = "/UPLOADONLY" Then
        ' upload file

        ' zip and ftp
        'FtpUpload
        ' upload to S3
        AwsS3Upload
    Else
        clsLogAutoProcess.Init
        clsLogAutoProcess.nProcessID = 66
        clsLogAutoProcess.LogAutoProcessStart
        clsLogAutoProcess.cProcessResult = "Incomplete"
        clsLogAutoProcess.cProcessNotes = ""
        
        nFile = FreeFile
        Open App.Path + "\OakReportsDataGen.txt" For Append As #nFile
        Print #nFile, "_________________________"
        Print #nFile, Format(Now, "DD/MM/YYYY - hh:mm:ss") + " Started"
        
        nLogFile = FreeFile
        Open App.Path + "\OakReportsLog.txt" For Append As #nLogFile
        Print #nLogFile, "_________________________"
        Print #nLogFile, Format(Now, "DD/MM/YYYY - hh:mm:ss") + " Started"
        
        nRenewalFile = FreeFile
        Open App.Path + "\OakReportsSubRenewals.txt" For Output As #nRenewalFile
        Print #nRenewalFile, "Pub,Issue,SubID,NonDD,NonDDDue2Ren,NonDDRen,NonDDRen2DD,DD,DDDue2Ren,DDRen"
        Print #nRenewalFile, Format(Now, "DD/MM/YYYY - hh:mm:ss")
    
        ' get last IDs
        cLastMailOrderID = "999999"
        If Dir(App.Path + "\oakreports.ini") <> "" Then
            If Not IsBlank(cIni.sGetINI(App.Path + "\oakreports.ini", "OakReports", "LastMailOrderID", "")) Then
                cLastMailOrderID = cIni.sGetINI(App.Path + "\oakreports.ini", "OakReports", "LastMailOrderID", "999999")
            End If
        End If
        cLastNewSubSourceIDList = "999999"
        If Dir(App.Path + "\oakreports.ini") <> "" Then
            If Not IsBlank(cIni.sGetINI(App.Path + "\oakreports.ini", "OakReports", "LastNewSubSourceID", "")) Then
                cLastNewSubSourceIDList = cIni.sGetINI(App.Path + "\oakreports.ini", "OakReports", "LastNewSubSourceID", "999999")
            End If
        End If
        cLastNewSubDate = "20000101"
        If Dir(App.Path + "\oakreports.ini") <> "" Then
            If Not IsBlank(cIni.sGetINI(App.Path + "\oakreports.ini", "OakReports", "LastNewSubDate", "")) Then
                cLastNewSubDate = cIni.sGetINI(App.Path + "\oakreports.ini", "OakReports", "LastNewSubDate", "999999")
            End If
        End If
        
        
        Set sCnn = New cConnection
        sCnn.OpenDB App.Path + "\KelseyOakReports.db"
        
        ReDim aDbfs(18) As String
        aDbfs(0) = "subscrip"
        aDbfs(1) = "payment"
        aDbfs(2) = "customer"
        aDbfs(3) = "pay_meth"
        aDbfs(4) = "liabmail"
        aDbfs(5) = "issue"
        aDbfs(6) = "sub_type"
        aDbfs(7) = "subs_aud"
        aDbfs(8) = "pay_proc"
        aDbfs(9) = "publish"
        aDbfs(10) = "source"
        aDbfs(11) = "country"
        aDbfs(12) = "batch"
        aDbfs(13) = "liab"
        aDbfs(14) = "morderh"
        aDbfs(15) = "morderl"
        aDbfs(16) = "product"
        aDbfs(17) = "issueadj"
        aDbfs(18) = "currency"
        Set data = Nothing
        cb = code4init()
        If Dir(App.Path + "\customer.dbf") <> "" Then
            Call DbfOpen(aDbfs, cb, data, App.Path, True, True, True)
        Else
            Call DbfOpen(aDbfs, cb, data, "c:\data\kelsey", True, True, True)
        End If
        
        Call d4tagSelect(data("subscrip"), data("tS_ID"))
        Call d4tagSelect(data("customer"), data("tC_ID"))
        Call d4tagSelect(data("payment"), data("tP_ID"))
        Call d4tagSelect(data("pay_proc"), data("tPP_PAYMENT"))
        Call d4tagSelect(data("pay_meth"), data("tPM_CODE"))
        Call d4tagSelect(data("liabmail"), data("tLM_PUB"))
        Call d4tagSelect(data("issue"), data("tISS_PUB"))
        Call d4tagSelect(data("sub_type"), data("tSUBT_PUB"))
        Call d4tagSelect(data("subs_aud"), data("tSAUD_SUBS"))
        Call d4tagSelect(data("publish"), data("tPUB_CODE"))
        Call d4tagSelect(data("source"), data("tSRCE_CODE"))
        Call d4tagSelect(data("country"), data("tCTRY_CODE"))
        Call d4tagSelect(data("morderh"), data("tMH_ID"))
        Call d4tagSelect(data("morderl"), data("tML_ORDER"))
        Call d4tagSelect(data("product"), data("tPR_CODE"))
        Call d4tagSelect(data("currency"), data("tCUR_CODE"))
           
        Set sTemp = New cConnection
        'sTemp.CreateNewDB App.Path + "\temp.db" '":memory:"
        sTemp.CreateNewDB ":memory:"
        sTemp.BeginTrans
        ' pay method
        sTemp.Execute "Create Table issuepay (ip_issuepay varchar(20), ip_pub varchar(6), ip_count integer, ip_value numeric(11,2), ip_new_count integer, ip_new_value numeric(11,2),ip_ren_count integer, ip_ren_value numeric(11,2))"
        sTemp.Execute "Create index ip_issuepay on issuepay (ip_issuepay,ip_pub)"
        ' issues resent by country
        'sTemp.Execute "Create Table issuesent (is_issue varchar(6), is_pub varchar(6), is_country varchar(3), is_resent integer)"
        ' agent sales
        'sTemp.Execute "Create Table agentsales (as_agent varchar(12), as_month varchar(6), as_pub varchar(6), as_subtype varchar(6), as_count integer, as_value numeric(11,2))"
        ' issue renewals
        sTemp.Execute "Create Table issuerenewal (ir_issue varchar(6), ir_pub varchar(6), ir_count integer, ir_non_cont_count integer, ir_non_cont_expire integer,ir_non_cont_renew1 integer, ir_non_cont_renew2 integer, ir_non_cont_renew numeric(6,2), ir_cont_count integer, ir_cont_expire integer, ir_cont_renew1 integer, ir_cont_renew numeric(6,2), ir_renewal_rate numeric(6,2), ir_subs_starting integer, ir_region varchar(3), ir_source varchar(12))"
        sTemp.Execute "Create index ir_issuepub on issuerenewal (ir_issue,ir_pub)"
        ' dd payments
        'sTemp.Execute "Create Table dd_count (dd_pub varchar(6), dd_offer varchar(6), dd_status varchar(1), dd_pay_count integer, dd_sub_count integer, dd_available varchar(1))"
        sTemp.Execute "create table mailorder (mo_order_id varchar(6),mo_product varchar(12),mo_date varchar(8),mo_qty integer, mo_value numeric(12,2));"
        sTemp.Execute "create index mo_product on mailorder(mo_product,mo_date)"
        sTemp.Execute "create table issue_adj (ia_pub varchar(6), ia_issue varchar(6), ia_reason varchar(6), ia_qty integer)"
        sTemp.Execute "create index ia_pub_issue_reason on issue_adj(ia_pub,ia_issue,ia_reason)"
        sTemp.CommitTrans

        sTemp.BeginTrans
        ' active subs pay method + non DD renewals + issues resent
        ' go through pub table
        d4top (data("publish"))
        Do While d4eof(data("publish")) = 0
            Label1 = Trim(fStr("PUB_CODE")) + " - " + Trim(fStr("PUB_DESC"))
            Label1.Refresh
            DoEvents
            cPub = fStr("PUB_CODE")
            nIssuesPerYear = f4int(data("PUB_ISS_YR"))
            ' get latest issue
            GetLatestIssues (cPub)
            ' reduce to just the last 3 months
            For nIssues = 0 To IIf(nIssuesPerYear > 24, 12, Round((nIssuesPerYear + 1) / 4))
                cIssue = aIssue(nIssues, 0)
                If cIssue >= cFirstIssue Then
                    Print #nLogFile, Trim(cPub) + "," + Trim(cIssue) + "-" + Format(Now, "YYYY-MM-DD hh:mm:ss")
                    Label1 = Trim(fStr("PUB_CODE")) + " - " + Trim(fStr("PUB_DESC")) + " : " + cIssue
                    Label1.Refresh
                    DoEvents
                    Call d4seek(data("liabmail"), cPub + cIssue)
                    Do While fStr("LM_PUB") = cPub And fStr("LM_ISSUE") = cIssue And d4eof(data("liabmail")) = 0
                        ' issue pay analysis
                        If d4seek(data("subscrip"), fStr("LM_SUBS")) = 0 Then
                            If d4seek(data("customer"), fStr("S_CUSTOMER")) = 0 Then
                                ' set region & source code
                                If Trim(fStr("C_COUNTRY")) = "UK" Then
                                    cRegion = "UK"
                                ElseIf Trim(fStr("C_COUNTRY")) = "USA" Or Trim(fStr("C_COUNTRY")) = "CAN" Then
                                    cRegion = "USA"
                                ElseIf d4seek(data("country"), fStr("C_COUNTRY")) = 0 Then
                                    cRegion = Trim(fStr("CTRY_OFFER"))
                                End If
                            Else
                                ' cust not found - set as UK
                                cRegion = "UK"
                            End If
                            cSource = Trim(fStr("S_SOURCE"))
    
                            ' issue renewal
                            For i = 0 To 7
                                ' 0= count
                                ' 1= non cont count
                                ' 2= non cont expires
                                ' 3= non cont renew to non cont
                                ' 4= non cont renew to cont
                                ' 5= DD cont count
                                ' 6= 'expire' with this issue
                                ' 7= DD cont renewed
                                ' 11 = subs starting this issue
                                aRenewal(i) = 0
                            Next i
                            aRenewal(11) = 0
                            If Trim(fStr("S_PAY_TYPE")) <> "FOC" Then
                                If fStr("S_STARTISS") = cIssue Then
                                    aRenewal(11) = 1
                                End If
                                aRenewal(0) = 1
                                If fStr("S_CONTIN") = "Y" Then
                                    aRenewal(5) = 1
                                    If fStr("S_ACTIVE") <> "A" Then
                                        ' not active so expiry issue = s_lastsent
                                        If fStr("S_LASTSENT") = fStr("LM_ISSUE") Then
                                            ' expires this issue & not renewed
                                            aRenewal(6) = 1
                                            aRenewal(7) = 0
                                        End If
                                    Else
                                        ' active DDs handled elsewhere - ie not by issue number
                                    End If
                                    
    
                                Else
                                    aRenewal(1) = 1
                                    If fStr("S_ENDISS") = fStr("LM_ISSUE") Then
                                        aRenewal(2) = 1
                                        If Not IsBlank(fStr("S_RENEWID")) Then
                                            If d4seek(data("subscrip"), fStr("S_RENEWID")) = 0 Then
                                                If fStr("S_CONTIN") = "Y" Then
                                                    aRenewal(4) = 1
                                                Else
                                                    aRenewal(3) = 1
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                                'Print #nRenewalFile, Trim(cPub) + "," + Trim(cIssue) + "," + fStr("S_ID") + "," + IIf(aRenewal(1) = 1, "Y", "") + "," + IIf(aRenewal(2) = 1, "Y", "") + "," + IIf(aRenewal(3) = 1, "Y", "") + "," + IIf(aRenewal(4) = 1, "Y", "") + "," + IIf(aRenewal(5) = 1, "Y", "") + "," + IIf(aRenewal(6) = 1, "Y", "") + "," + IIf(aRenewal(7) = 1, "Y", "")
                                Set rsTemp = sTemp.OpenRecordset("Select ir_count, ir_non_cont_count,ir_non_cont_expire,ir_non_cont_renew1, ir_non_cont_renew2, ir_cont_count, ir_cont_expire, ir_cont_renew1,ir_subs_starting from issuerenewal where ir_issue='" + cIssue + "' and ir_pub='" + Trim(cPub) + "' and ir_region='" + Trim(cRegion) + "' and ir_source='" + Trim(cSource) + "'")
                                If rsTemp.RecordCount = 0 Then
                                    sTemp.Execute "Insert into issuerenewal values('" + cIssue + "','" + Trim(cPub) + "'," + Str(aRenewal(0)) + "," + Str(aRenewal(1)) + "," + Str(aRenewal(2)) + "," + Str(aRenewal(3)) + "," + Str(aRenewal(4)) + ",0," + Str(aRenewal(5)) + "," + Str(aRenewal(6)) + "," + Str(aRenewal(7)) + ",0,0," + Str(aRenewal(11)) + ",'" + cRegion + "','" + cSource + "')"
                                ElseIf rsTemp.RecordCount > 1 Then
                                Else
                                    aRenewal(0) = Val(rsTemp.Fields("ir_count")) + aRenewal(0)
                                    aRenewal(1) = Val(rsTemp.Fields("ir_non_cont_count")) + aRenewal(1)
                                    aRenewal(2) = Val(rsTemp.Fields("ir_non_cont_expire")) + aRenewal(2)
                                    aRenewal(3) = Val(rsTemp.Fields("ir_non_cont_renew1")) + aRenewal(3)
                                    aRenewal(4) = Val(rsTemp.Fields("ir_non_cont_renew2")) + aRenewal(4)
                                    aRenewal(5) = Val(rsTemp.Fields("ir_cont_count")) + aRenewal(5)
                                    aRenewal(6) = Val(rsTemp.Fields("ir_cont_expire")) + aRenewal(6)
                                    aRenewal(7) = Val(rsTemp.Fields("ir_cont_renew1")) + aRenewal(7)
                                    aRenewal(11) = Val(rsTemp.Fields("ir_subs_starting")) + aRenewal(11)
                                    sTemp.Execute "update issuerenewal set ir_count=" + Str(aRenewal(0)) + ", ir_non_cont_count=" + Str(aRenewal(1)) + ", ir_non_cont_expire=" + Str(aRenewal(2)) + ", ir_non_cont_renew1=" + Str(aRenewal(3)) + ", ir_non_cont_renew2=" + Str(aRenewal(4)) + ",ir_cont_count=" + Str(aRenewal(5)) + ", ir_cont_expire=" + Str(aRenewal(6)) + ", ir_cont_renew1=" + Str(aRenewal(7)) + ",ir_subs_starting=" + Str(aRenewal(11)) + " where ir_issue='" + cIssue + "' and ir_pub='" + Trim(cPub) + "' and ir_region='" + Trim(cRegion) + "' and ir_source='" + Trim(cSource) + "'"
                                End If
                            End If
                        End If
                        
                        Call d4skip(data("liabmail"), 1)
                    Loop
                End If
            Next nIssues
            Call d4skip(data("publish"), 1)
        Loop
        sTemp.CommitTrans
        clsLogAutoProcess.cProcessNotes = clsLogAutoProcess.cProcessNotes + "SubsByPayMethod + IssuesResent generated " + Format(Now, "YYMMDD hh:mm:ss") + vbCrLf
           
        ' go through DD payments for renewal analysis
        sTemp.BeginTrans
        Call d4tagSelect(data("payment"), data("tP_DDREF"))
        Call d4tagSelect(data("subscrip"), data("tS_PAY_ID"))
        Print #nLogFile, "Start DD -" + Format(Now, "YYMMDD hh:mm:ss")
        d4top (data("payment"))
        i = 0
        Do While d4eof(data("payment")) = 0
            If d4seek(data("subscrip"), fStr("P_ID")) = 0 Then
                nCount = 0
                cPub = fStr("S_PUBLICAT")
                cIssue = fStr("S_LASTSENT")
                If IsBlank(cIssue) Then
                    cIssue = fStr("S_STARTISS")
                End If
                If d4seek(data("customer"), fStr("S_CUSTOMER")) = 0 Then
                    ' set region & source code
                    If Trim(fStr("C_COUNTRY")) = "UK" Then
                        cRegion = "UK"
                    ElseIf Trim(fStr("C_COUNTRY")) = "USA" Or Trim(fStr("C_COUNTRY")) = "CAN" Then
                        cRegion = "USA"
                    ElseIf d4seek(data("country"), fStr("C_COUNTRY")) = 0 Then
                        cRegion = Trim(fStr("CTRY_OFFER"))
                    End If
                Else
                    ' cust not found - set as UK
                    cRegion = "UK"
                End If
                cSource = Trim(fStr("S_SOURCE"))
                        
                cFirstDD = ""
                Call d4seek(data("pay_proc"), fStr("p_id"))
                Do While fStr("PP_PAYMENT") = fStr("P_ID") And d4eof(data("payment")) = 0
                    If Left(fStr("PP_STATUS"), 1) = "A" Then
                        ' set 1st date
                        cFirstDD = fStr("PP_DATE")
                    End If
                    Call d4skip(data("pay_proc"), 1)
                Loop
                
                ' we now know the 1st DD date
                Call d4seek(data("pay_proc"), fStr("p_id"))
                Do While fStr("PP_PAYMENT") = fStr("P_ID") And d4eof(data("payment")) = 0
                    If Left(fStr("PP_STATUS"), 1) = "A" And fStr("PP_DATE") > cFirstDD Then
                        ' this is a renewal - find relevant pub/issue
                        If d4seek(data("issue"), cPub + cIssue) = 0 Then
                            If fStr("ISS_MAIL") < fStr("PP_DATE") Then
                                ' go forward until mailing date past payment
                                Do While fStr("ISS_MAIL") < fStr("PP_DATE")
                                    cIssue = fStr("ISS_NUMBER")
                                    Call d4skip(data("issue"), 1)
                                Loop
                            ElseIf fStr("ISS_MAIL") = fStr("PP_DATE") Then
                                cIssue = fStr("ISS_NUMBER")
                            Else
                                ' mailing ahead of payment so go back
                                Do While fStr("ISS_MAIL") > fStr("PP_DATE")
                                    Call d4skip(data("issue"), -1)
                                    ' skip back 1st as we want to be before the payment date
                                    cIssue = fStr("ISS_NUMBER")
                                Loop
                            End If
                        End If
                        ' cIssue = expiry date when sub was renewed
                        Set rsTemp = sTemp.OpenRecordset("Select * from issuerenewal where ir_issue='" + cIssue + "' and ir_pub='" + Trim(cPub) + "' and ir_region='" + Trim(cRegion) + "' and ir_source='" + Trim(cSource) + "'")
                        If rsTemp.RecordCount = 0 Then
                            ' shouldn't happen as we've found in previous renewal analysis
                            ' will happen for old issue before cut-off which is fine
                        ElseIf rsTemp.RecordCount > 1 Then
                        Else
                            ' only need to increase DD expiry & renewal count
                            sTemp.Execute "update issuerenewal set ir_cont_expire=ir_cont_expire+1, ir_cont_renew1=ir_cont_renew1+1 where ir_issue='" + cIssue + "' and ir_pub='" + Trim(cPub) + "' and ir_region='" + Trim(cRegion) + "' and ir_source='" + Trim(cSource) + "'"
                        End If
                    End If
                    Call d4skip(data("pay_proc"), 1)
                Loop
    
            End If
            Call d4skip(data("payment"), 1)
            Label1 = Str(i) + " - " + fStr("P_DDREF")
            Label1.Refresh
            i = i + 1
            If Round(i / 1000) = i / 1000 Then
                DoEvents
            End If
        Loop
        Print #nLogFile, "End DD -" + Format(Now, "YYMMDD hh:mm:ss")
        sTemp.CommitTrans
        clsLogAutoProcess.cProcessNotes = clsLogAutoProcess.cProcessNotes + "DD counts generated " + Format(Now, "YYMMDD hh:mm:ss") + vbCrLf
        
        ' go through mailing batches - mailed value + future lapsed + new sub response
        sCnn.BeginTrans
        Call d4tagSelect(data("batch"), data("tBAT_ID"))
        Call d4tagSelect(data("subscrip"), data("tS_ID"))
        Call d4tagSelect(data("customer"), data("tC_ID"))
        Call d4tagSelect(data("payment"), data("tP_ID"))
        Call d4tagSelect(data("liabmail"), data("tLM_LIABID"))
        Call d4seek(data("batch"), "M001339") ' 2334 is first batch in 2018 1339 1st in 2017
        Dim aMVExport As New Collection
        Dim aExportSplit() As String
        Dim cTempExport As String, vKey As Variant
        Dim aMV(9) As Double
        ' 0 = dd uk count
        ' 1 = non dd uk count
        ' 2 = dd os count
        ' 3 = non dd os count
        ' 4 = paid
        ' 5 = foc
        ' 6 - mailed UK value
        ' 7 - mailed os value
        ' 8 - ccc uk count
        ' 9 - ccc os count
        Dim aAS(10, 2) As Integer ' 0=print, 1=digital, 2=print+digital
        ' 0 count
        ' 1 uk count
        ' 2 eu count
        ' 3 us count
        ' 4 row count
        ' 5 gift count
        ' 6 direct count
        ' 7 dd count
        ' 8 non dd count
        ' 9 ccc count
        ' 10 foc count
        Dim aFL(1 To 50, 3) As Integer
        ' 0 uk
        ' 1 os
        ' 2 gift
        ' 3 direct
        Dim aNSR(7) As Integer
        ' 0 blank or UNKNOW
        ' 1 3RDPTY
        ' 2 EMAIL
        ' 3 EVENT
        ' 4 OLD
        ' 5 POST
        ' 6 TEL
        ' 7 WEB
        'Dim aNSS(1 To 50, 7) As Variant
        ' 0 source code
        ' 1 direct
        ' 2 gift
        ' 3 uk
        ' 4 os
        ' 5 dd count
        ' 6 non dd/ccc count
        ' 7 ccc count
        Print #nLogFile, "Start mailing batches -" + Format(Now, "YYMMDD hh:mm:ss")
        Do While fStr("BAT_TYPE") = "M"
            If fStr("BAT_DATE") >= "20170101" Then     '"20150930"
                Set rs = sCnn.OpenRecordset("select mv_id from mailed_value where mv_batch_id='" + Trim(fStr("BAT_BATCH")) + "'")
                If rs.RecordCount = 0 Then
                    ' process batch
                    For i = 0 To 9
                        aMV(i) = 0
                    Next i
                    For j = 0 To 2
                        For i = 0 To 10
                            aAS(i, j) = 0
                        Next i
                    Next j
                    For i = 0 To 7
                        aNSR(i) = 0
                    Next i
                    For i = 1 To 50
                        For j = 0 To 3
                            aFL(i, j) = 0
                        Next j
                        'aNSS(i, 0) = ""
                        'For j = 1 To 7
                        '    aNSS(i, j) = 0
                        'Next j
                    Next i
                    Set aMVExport = Nothing
                    Set aMVExport = New Collection
                    cLatestIssue = GetLatestIssue(fStr("BAT_PUB"))
                    If d4seek(data("liabmail"), fStr("BAT_SCID")) = 0 Then
                        Do While fStr("LM_LIABID") = fStr("BAT_SCID") And d4eof(data("liabmail")) = 0
                            If fStr("LM_PUB") = fStr("BAT_PUB") Then
                                If d4seek(data("subscrip"), fStr("LM_SUBS")) = 0 Then
                                    Call d4seek(data("sub_type"), fStr("S_PUBLICAT") + fStr("S_SUBTYPE"))
                                    nType = IIf(fStr("SUBT_DIGIT") = "Y", 1, IIf(fStr("SUBT_PRDIG") = "Y", 2, 0))
                                    lActive = (fStr("S_ACTIVE") = "A" Or Not IsBlank(fStr("S_RENEWID"))) ' only count record
                                    If d4seek(data("customer"), fStr("S_CUSTOMER")) = 0 And (d4seek(data("payment"), fStr("S_PAY_ID")) = 0 Or Trim(fStr("S_PAY_TYPE")) = "FOC" Or Trim(fStr("S_SOURCE")) = "TRANSFER") Then
                                        If Trim(fStr("C_COUNTRY")) = "UK" Then
                                            If Trim(fStr("S_PAY_TYPE")) = "FOC" Then
                                                aMV(5) = aMV(5) + 1
                                            Else
                                                aMV(4) = aMV(4) + 1
                                                If Trim(fStr("P_METHOD")) = "DD" Then
                                                    aMV(0) = aMV(0) + 1
                                                ElseIf Trim(fStr("P_METHOD")) = "WEBCCC" Then
                                                    aMV(8) = aMV(8) + 1
                                                Else
                                                    aMV(1) = aMV(1) + 1
                                                End If
                                            End If
                                            aAS(1, nType) = aAS(1, nType) + 1 'IIf(lActive, 1, 0)
                                            aMV(6) = aMV(6) + f4double(data("LM_AMOUNT"))
                                        Else
                                            If Trim(fStr("S_PAY_TYPE")) = "FOC" Then
                                                aMV(5) = aMV(5) + 1
                                            Else
                                                aMV(4) = aMV(4) + 1
                                                If Trim(fStr("P_METHOD")) = "DD" Then
                                                    aMV(2) = aMV(2) + 1
                                                ElseIf Trim(fStr("P_METHOD")) = "WEBCCC" Then
                                                    aMV(9) = aMV(9) + 1
                                                Else
                                                    aMV(3) = aMV(3) + 1
                                                End If
                                            End If
                                            'If Trim(fStr("C_COUNTRY")) = "USA" Then
                                            If Trim(fStr("C_COUNTRY")) = "USA" Or Trim(fStr("C_COUNTRY")) = "CAN" Then
                                                aAS(3, nType) = aAS(3, nType) + 1 'IIf(lActive, 1, 0)
                                            Else
                                                If d4seek(data("country"), fStr("C_COUNTRY")) = 0 Then
                                                    If fStr("CTRY_OFFER") = "EUR" Then
                                                        aAS(2, nType) = aAS(2, nType) + 1 'IIf(lActive, 1, 0)
                                                    Else
                                                        aAS(4, nType) = aAS(4, nType) + 1 'IIf(lActive, 1, 0)
                                                    End If
                                                End If
                                            End If
                                            aMV(7) = aMV(7) + f4double(data("LM_AMOUNT"))
                                            ' set mv export values
                                            If KeyExists(aMVExport, Trim(fStr("C_COUNTRY"))) Then
                                                ' add to existing key
                                                aExportSplit = Split(CStr(aMVExport(Trim(fStr("C_COUNTRY")))(1)), "-")
                                                aExportSplit(0) = aExportSplit(0) + 1
                                                aExportSplit(1) = aExportSplit(1) + f4double(data("LM_AMOUNT"))
                                                If Trim(fStr("P_METHOD")) = "WEBCCC" Then
                                                    aExportSplit(2) = aExportSplit(2) + 1
                                                    aExportSplit(3) = aExportSplit(3) + f4double(data("LM_AMOUNT"))
                                                End If
                                                aMVExport.Remove (Trim(fStr("C_COUNTRY")))
                                                aMVExport.Add Array(Trim(fStr("C_COUNTRY")), Trim(CStr(aExportSplit(0))) + "-" + Trim(CStr(aExportSplit(1))) + "-" + Trim(CStr(aExportSplit(2))) + "-" + Trim(CStr(aExportSplit(3)))), Trim(fStr("C_COUNTRY"))
                                            Else
                                                ' add new key
                                                aMVExport.Add Array(Trim(fStr("C_COUNTRY")), "1-" + Trim(Str(f4double(data("LM_AMOUNT")))) + "-" + IIf(Trim(fStr("P_METHOD")) = "WEBCCC", "1-" + Trim(Str(f4double(data("LM_AMOUNT")))), "0-0")), Trim(fStr("C_COUNTRY"))
                                            End If
                                        End If
                                        If Trim(fStr("S_PAY_TYPE")) = "FOC" Then
                                            aAS(10, nType) = aAS(10, nType) + 1
                                        ElseIf Trim(fStr("P_METHOD")) = "DD" Then
                                            aAS(7, nType) = aAS(7, nType) + 1 'IIf(lActive, 1, 0)
                                        ElseIf Trim(fStr("P_METHOD")) = "WEBCCC" Then
                                            aAS(9, nType) = aAS(9, nType) + 1 'IIf(lActive, 1, 0)
                                        Else
                                            aAS(8, nType) = aAS(8, nType) + 1 'IIf(lActive, 1, 0)
                                        End If
                                        If IsBlank(fStr("S_DONORID")) Then
                                            aAS(6, nType) = aAS(6, nType) + 1 'IIf(lActive, 1, 0)
                                        Else
                                            aAS(5, nType) = aAS(5, nType) + 1 'IIf(lActive, 1, 0)
                                        End If
                                    End If
                                    ' future lapsed
                                    If fStr("S_CONTIN") = "N" And Trim(fStr("S_PAY_TYPE")) <> "FOC" Then
                                        nIssues = f4int(data("S_ISS_LEFT"))
                                        If lActive And nIssues > 0 And nIssues < 50 Then
                                            If Trim(fStr("C_COUNTRY")) = "UK" Then
                                                aFL(nIssues, 0) = aFL(nIssues, 0) + 1
                                            Else
                                                aFL(nIssues, 1) = aFL(nIssues, 1) + 1
                                            End If
                                            If IsBlank(fStr("S_DONORID")) Then
                                                aFL(nIssues, 3) = aFL(nIssues, 3) + 1
                                            Else
                                                aFL(nIssues, 2) = aFL(nIssues, 2) + 1
                                            End If
                                        End If
                                    End If
                                    ' new subs response + new sub source code analysis - check if its 1st issue mailed
                                    If fStr("BAT_ISSUE") = fStr("S_STARTISS") Then
                                        ' channel
                                        Select Case Trim(fStr("S_UDF1"))
                                        Case "3RDPTY"
                                            aNSR(1) = aNSR(1) + 1
                                        Case "EMAIL"
                                            aNSR(2) = aNSR(2) + 1
                                        Case "EVENT"
                                            aNSR(3) = aNSR(3) + 1
                                        Case "OLD"
                                            aNSR(4) = aNSR(4) + 1
                                        Case "POST"
                                            aNSR(5) = aNSR(5) + 1
                                        Case "TEL"
                                            aNSR(6) = aNSR(6) + 1
                                        Case "WEB"
                                            aNSR(7) = aNSR(7) + 1
                                        Case Else
                                            aNSR(0) = aNSR(0) + 1
                                        End Select
                                        
                                        ' source code - new not renewed subs only
                                        'cNewRenewed = IIf(IsBlank(fStr("S_RENEWOF")), "N", "R")
                                        'nSource = 0
                                        'For i = 1 To 50
                                        '    If IsBlank(CStr(aNSS(i, 0))) Then
                                        '        aNSS(i, 0) = fStr("S_SOURCE") + cNewRenewed
                                        '        nSource = i
                                        '    ElseIf CStr(aNSS(i, 0)) = fStr("S_SOURCE") + cNewRenewed Then
                                        '        nSource = i
                                        '    End If
                                        '    If nSource > 0 Then
                                        '        nSource = i
                                        '        If IsBlank(fStr("S_DONORID")) Then
                                        '            aNSS(nSource, 1) = aNSS(nSource, 1) + 1
                                        '        Else
                                        '            aNSS(nSource, 2) = aNSS(nSource, 2) + 1
                                        '        End If
                                        '        If Trim(fStr("C_COUNTRY")) = "UK" Then
                                        '            aNSS(nSource, 3) = aNSS(nSource, 3) + 1
                                        '        Else
                                        '            aNSS(nSource, 4) = aNSS(nSource, 4) + 1
                                        '        End If
                                        '        If Trim(fStr("P_METHOD")) = "DD" Then
                                        '            aNSS(nSource, 5) = aNSS(nSource, 5) + 1
                                        '        ElseIf Trim(fStr("P_METHOD")) = "WEBCCC" Then
                                        '            aNSS(nSource, 7) = aNSS(nSource, 7) + 1
                                        '        Else
                                        '            aNSS(nSource, 6) = aNSS(nSource, 6) + 1
                                        '        End If
                                        '        i = 50
                                        '    End If
                                        'Next i
                                    End If
                                End If
                            End If
                            Call d4skip(data("liabmail"), 1)
                        Loop
                    End If
                    
                    cTempExport = ""
                    For Each vKey In aMVExport
                        cTempExport = cTempExport + CStr(vKey(0)) + ":" + CStr(vKey(1)) + ";"
                    Next
                    cSql = "insert into mailed_value (mv_batch_id,mv_pub,mv_issue, mv_count, mv_dd_uk_count,mv_non_dd_uk_count,mv_dd_os_count,mv_non_dd_os_count,mv_paid_count, mv_foc_count, mv_mailed_value, mv_mailed_value_uk, mv_mailed_value_os,mv_ccc_uk_count,mv_ccc_os_count,mv_os_breakdown) values ("
                    cSql = cSql + "'" + fStr("BAT_BATCH") + "','" + Trim(fStr("BAT_PUB")) + "','" + Trim(fStr("BAT_ISSUE")) + "'," + Str(aMV(4) + aMV(5)) + "," + Str(aMV(0)) + "," + Str(aMV(1)) + "," + Str(aMV(2)) + "," + Str(aMV(3)) + "," + Str(aMV(4)) + "," + Str(aMV(5)) + "," + Str(Round(aMV(6) + aMV(7), 2)) + "," + Str(Round(aMV(6), 2)) + "," + Str(Round(aMV(7), 2)) + "," + Str(aMV(8)) + "," + Str(aMV(9)) + ",'" + cTempExport + "')"
                    sCnn.Execute (cSql)
                
                    ' add print, digital & print+digital records
                    cSql = "insert into active_subs (as_batch_id,as_mailing,as_pub,as_issue,as_type, as_count, as_uk_count,as_eu_count,as_us_count,as_row_count,as_gift_count, as_direct_count, as_dd_count,as_non_dd_count,as_ccc_count,as_foc_count) values ("
                    cSql = cSql + "'" + fStr("BAT_BATCH") + "','Y','" + Trim(fStr("BAT_PUB")) + "','" + Trim(fStr("BAT_ISSUE")) + "','P'," + Str(aAS(7, 0) + aAS(8, 0) + aAS(9, 0))
                    For i = 1 To 10
                        cSql = cSql + "," + Str(aAS(i, 0))
                    Next i
                    cSql = cSql + ")"
                    sCnn.Execute (cSql)
                    cSql = "insert into active_subs (as_batch_id,as_mailing,as_pub,as_issue,as_type, as_count, as_uk_count,as_eu_count,as_us_count,as_row_count,as_gift_count, as_direct_count, as_dd_count,as_non_dd_count,as_ccc_count,as_foc_count) values ("
                    cSql = cSql + "'" + fStr("BAT_BATCH") + "','Y','" + Trim(fStr("BAT_PUB")) + "','" + Trim(fStr("BAT_ISSUE")) + "','D'," + Str(aAS(7, 1) + aAS(8, 1) + aAS(9, 1))
                    For i = 1 To 10
                        cSql = cSql + "," + Str(aAS(i, 1))
                    Next i
                    cSql = cSql + ")"
                    sCnn.Execute (cSql)
                    cSql = "insert into active_subs (as_batch_id,as_mailing,as_pub,as_issue,as_type, as_count, as_uk_count,as_eu_count,as_us_count,as_row_count,as_gift_count, as_direct_count, as_dd_count,as_non_dd_count,as_ccc_count,as_foc_count) values ("
                    cSql = cSql + "'" + fStr("BAT_BATCH") + "','Y','" + Trim(fStr("BAT_PUB")) + "','" + Trim(fStr("BAT_ISSUE")) + "','B'," + Str(aAS(7, 2) + aAS(8, 2) + aAS(9, 2))
                    For i = 1 To 10
                        cSql = cSql + "," + Str(aAS(i, 2))
                    Next i
                    cSql = cSql + ")"
                    sCnn.Execute (cSql)
                    
                    Call d4seek(data("issue"), fStr("BAT_PUB") + fStr("BAT_ISSUE"))
                    For i = 1 To 50
                        Call d4skip(data("issue"), 1) ' move to the future issue
                        If fStr("ISS_PUB") = fStr("BAT_PUB") Then
                            cFutureIssue = Trim(fStr("ISS_NUMBER"))
                        Else
                            cFutureIssue = ""
                        End If
                        cSql = "insert into future_lapsed (fl_batch_id,fl_pub,fl_issue, fl_issues_remaining,fl_future_issue,fl_uk_count, fl_os_count,fl_gift_count, fl_direct_count) values ("
                        cSql = cSql + "'" + fStr("BAT_BATCH") + "','" + Trim(fStr("BAT_PUB")) + "','" + Trim(fStr("BAT_ISSUE")) + "'," + Str(i) + ",'" + cFutureIssue + "'," + Str(aFL(i, 0)) + "," + Str(aFL(i, 1)) + "," + Str(aFL(i, 2)) + "," + Str(aFL(i, 3)) + ")"
                        sCnn.Execute (cSql)
                    Next i
                    
                    For i = 0 To 7
                        Select Case i
                        Case 0
                            cChannel = "Unknown"
                        Case 1
                            cChannel = "3rd Party"
                        Case 2
                            cChannel = "Email"
                        Case 3
                            cChannel = "Event"
                        Case 4
                            cChannel = "Old"
                        Case 5
                            cChannel = "Post"
                        Case 6
                            cChannel = "Telephone"
                        Case 7
                            cChannel = "Web"
                        End Select
                        cSql = "insert into new_subs_response (nsr_batch_id, nsr_pub,nsr_issue,nsr_channel,nsr_count) values ("
                        cSql = cSql + "'" + fStr("BAT_BATCH") + "','" + Trim(fStr("BAT_PUB")) + "','" + Trim(fStr("BAT_ISSUE")) + "','" + cChannel + "'," + Str(aNSR(i)) + ")"
                        sCnn.Execute (cSql)
                    Next i
                    
                    'For i = 1 To 50
                    '    If IsBlank(CStr(aNSS(i, 0))) Then
                    '        ' no source code stored
                    '        i = 50
                    '    Else
                    '        cSql = "insert into new_subs_source (nss_batch_id,nss_pub,nss_source,nss_new_renewed,nss_direct_count,nss_gift_count,nss_uk_count,nss_os_count,nss_dd_count,nss_non_dd_count,nss_ccc_count) values ("
                    '        cSql = cSql + "'" + fStr("BAT_BATCH") + "','" + Trim(fStr("BAT_PUB")) + "','" + Trim(Left(aNSS(i, 0), 12)) + "','" + Right(aNSS(i, 0), 1) + "'," + CStr(aNSS(i, 1)) + "," + CStr(aNSS(i, 2)) + "," + CStr(aNSS(i, 3)) + "," + CStr(aNSS(i, 4)) + "," + CStr(aNSS(i, 5)) + "," + CStr(aNSS(i, 6)) + "," + CStr(aNSS(i, 7)) + ")"
                    '        sCnn.Execute (cSql)
                    '    End If
                    'Next i
                End If
            End If
            Call d4skip(data("batch"), 1)
        Loop
        sCnn.CommitTrans
        Print #nLogFile, "End mailing batches -" + Format(Now, "YYMMDD hh:mm:ss")

        ' get latest active subs since last mailing
        ' 0 count
        ' 1 uk count
        ' 2 eu count
        ' 3 us count
        ' 4 row count
        ' 5 gift count
        ' 6 direct count
        ' 7 dd count
        ' 8 non dd non ccc count
        ' 9 ccc count
        ' 10 foc count
        Print #nLogFile, "Start subs since last mailing -" + Format(Now, "YYMMDD hh:mm:ss")
        Call d4tagSelect(data("issue"), data("tISS_PUB"))
        Call d4tagSelect(data("subscrip"), data("tS_PUBLICAT"))
        Call d4tagSelect(data("liabmail"), data("tLM_SUBS"))
        Call d4tagSelect(data("liab"), data("tL_ID"))
        sCnn.BeginTrans
        d4top (data("publish"))
        Do While d4eof(data("publish")) = 0
            Call d4tagSelect(data("subscrip"), data("tS_PUBLICAT"))
            Call d4seek(data("issue"), fStr("PUB_CODE"))
            'cMailDate = Format(DateAdd("d", -60, Now), "YYYYMMDD") ' 60 days ago
            cIssue = ""
            Do While Not IsBlank(fStr("ISS_MAILUK"))
                cMailDate = fStr("ISS_MAILUK")
                cIssue = Trim(fStr("ISS_NUMBER"))
                Call d4skip(data("issue"), 1)
            Loop
            For j = 0 To 2
                For i = 0 To 10
                    aAS(i, j) = 0
                Next i
            Next j
            
            ' find subs created on that day or later - find 1st sub for 'next' pub and go backwards
            Call d4seek(data("subscrip"), Trim(fStr("PUB_CODE")) + "X  ")
            Call d4skip(data("subscrip"), -1)
            Do While fStr("S_PUBLICAT") = fStr("PUB_CODE") And fStr("S_CREATED") >= cMailDate
                Call d4seek(data("sub_type"), fStr("S_PUBLICAT") + fStr("S_SUBTYPE"))
                nType = IIf(fStr("SUBT_DIGIT") = "Y", 1, IIf(fStr("SUBT_PRDIG") = "Y", 2, 0))
                lInclude = (fStr("S_ACTIVE") = "A")
                If lInclude Then
                    If fStr("S_CREATED") = cMailDate Then
                        ' created on mailing date - check if mailed
                        If d4seek(data("liabmail"), fStr("S_ID") + cIssue) = 0 Then
                            ' issue mailed
                            If d4seek(data("liab"), fStr("LM_LIABID")) = 0 Then
                                Do While fStr("L_ID") = fStr("LM_LIABID")
                                    If fStr("L_PUB") = fStr("PUB_CODE") And InStr(fStr("L_REASON"), "Mailed") > 0 Then
                                        lInclude = False
                                    End If
                                    Call d4skip(data("liab"), 1)
                                Loop
                            End If
                        End If
                    End If
                End If
                If lInclude Then
                    ' check if its an advance renewal with start issue after last mailing ie the previous sub was in the latest mailing
                    If Not IsBlank(fStr("S_RENEWOF")) And fStr("S_STARTISS") > cIssue Then
                        If d4seek(data("liabmail"), fStr("S_RENEWOF") + cIssue) = 0 Then
                            ' issue mailed
                            lInclude = False
                        End If
                    End If
                End If
                If lInclude Then
                    If d4seek(data("customer"), fStr("S_CUSTOMER")) = 0 And d4seek(data("payment"), fStr("S_PAY_ID")) = 0 Then
                        If Trim(fStr("C_COUNTRY")) = "UK" Then
                            aAS(1, nType) = aAS(1, nType) + 1
                        Else
                            If Trim(fStr("C_COUNTRY")) = "USA" Or Trim(fStr("C_COUNTRY")) = "CAN" Then
                                aAS(3, nType) = aAS(3, nType) + 1
                            Else
                                If d4seek(data("country"), fStr("C_COUNTRY")) = 0 Then
                                    If fStr("CTRY_OFFER") = "EUR" Then
                                        aAS(2, nType) = aAS(2, nType) + 1
                                    Else
                                        aAS(4, nType) = aAS(4, nType) + 1
                                    End If
                                End If
                            End If
                        End If
                        If Trim(fStr("S_PAY_TYPE")) = "FOC" Then
                            aAS(10, nType) = aAS(10, nType) + 1
                        ElseIf Trim(fStr("P_METHOD")) = "DD" Then
                            aAS(7, nType) = aAS(7, nType) + 1
                        ElseIf Trim(fStr("P_METHOD")) = "WEBCCC" Then
                            aAS(9, nType) = aAS(9, nType) + 1
                        Else
                            aAS(8, nType) = aAS(8, nType) + 1
                        End If
                        If IsBlank(fStr("S_DONORID")) Then
                            aAS(6, nType) = aAS(6, nType) + 1
                        Else
                            aAS(5, nType) = aAS(5, nType) + 1
                        End If
                    End If
                    
'                    ' update new subs source details
'                    For j = 1 To 7
'                        aNSS(1, j) = 0
'                    Next j
'                    ' source code - new not renewed subs only
'                    cNewRenewed = IIf(IsBlank(fStr("S_RENEWOF")), "N", "R")
'                    aNSS(1, 0) = fStr("S_SOURCE") + cNewRenewed
'                    nSource = i
'                    If IsBlank(fStr("S_DONORID")) Then
'                        aNSS(1, 1) = 1
'                    Else
'                        aNSS(1, 2) = 1
'                    End If
'                    If Trim(fStr("C_COUNTRY")) = "UK" Then
'                        aNSS(1, 3) = 1
'                    Else
'                        aNSS(1, 4) = 1
'                    End If
'                    If Trim(fStr("P_METHOD")) = "DD" Then
'                        aNSS(1, 5) = 1
'                    ElseIf Trim(fStr("P_METHOD")) = "WEBCCC" Then
'                        aNSS(1, 7) = 1
'                    Else
'                        aNSS(1, 6) = 1
'                    End If
'                    ' update database
'                    Set rs = sCnn.OpenRecordset("select nss_id from new_subs_source where nss_pub='" + Trim(fStr("S_PUBLICAT")) + "' and nss_source='" + Trim(fStr("S_SOURCE")) + "' and nss_date='" + fStr("S_ORDDATE") + "'")
'                    If rs.RecordCount = 0 Then
'                        ' add record
'                        cSql = "insert into new_subs_source (nss_pub,nss_source,nss_new_renewed,nss_direct_count,nss_gift_count,nss_uk_count,nss_os_count,nss_dd_count,nss_non_dd_count,nss_ccc_count,nss_date) values ("
'                        cSql = cSql + "'" + Trim(fStr("S_PUBLICAT")) + "','" + Trim(Left(aNSS(1, 0), 12)) + "','" + Right(aNSS(1, 0), 1) + "'," + CStr(aNSS(1, 1)) + "," + CStr(aNSS(1, 2)) + "," + CStr(aNSS(1, 3)) + "," + CStr(aNSS(1, 4)) + "," + CStr(aNSS(1, 5)) + "," + CStr(aNSS(1, 6)) + "," + CStr(aNSS(1, 7)) + "," + fStr("S_ORDDATE") + ")"
'                        sCnn.Execute (cSql)
'                    Else
'                        ' update record with values
'                        cSql = "update new_subs_source set nss_direct_count=" + Str(aNSS(1, 1)) + ",nss_gift_count=" + Str(aNSS(1, 2)) + ",nss_uk_count=" + Str(aNSS(1, 3)) + ",nss_os_count=" + Str(aNSS(1, 4)) + ",nss_dd_count=" + Str(aNSS(1, 5)) + ",nss_non_dd_count=" + Str(aNSS(1, 6)) + ",nss_ccc_count=" + Str(aNSS(1, 7))
'                        cSql = cSql + " where nss_pub='" + Trim(fStr("S_PUBLICAT")) + "' and nss_source='" + Trim(fStr("S_SOURCE")) + "' and nss_new_renewed='" + Right(aNSS(1, 0), 1) + "' and nss_date='" + fStr("S_ORDDATE") + "'"
'                        sCnn.Execute (cSql)
'                    End If
                End If
                Call d4skip(data("subscrip"), -1)
            Loop
            ' check any cancelled subs
            Call d4tagSelect(data("subscrip"), data("tS_CANCDATE"))
            Call d4seek(data("subscrip"), cMailDate)
            Do While d4eof(data("subscrip")) = 0
                If fStr("S_CANCDATE") > cMailDate Then ' don't remove from subs if date = mailing date as likely this was cancelled due to FUTCAN last mailing
                    If fStr("S_PUBLICAT") = fStr("PUB_CODE") Then
                        If d4seek(data("customer"), fStr("S_CUSTOMER")) = 0 And d4seek(data("payment"), fStr("S_PAY_ID")) = 0 Then
                            If Trim(fStr("C_COUNTRY")) = "UK" Then
                                aAS(1, nType) = aAS(1, nType) - 1
                            Else
                                If Trim(fStr("C_COUNTRY")) = "USA" Then
                                    aAS(3, nType) = aAS(3, nType) - 1
                                Else
                                    If d4seek(data("country"), fStr("C_COUNTRY")) = 0 Then
                                        If fStr("CTRY_OFFER") = "EUR" Then
                                            aAS(2, nType) = aAS(2, nType) - 1
                                        Else
                                            aAS(4, nType) = aAS(4, nType) - 1
                                        End If
                                    End If
                                End If
                            End If
                            If Trim(fStr("P_METHOD")) = "DD" Then
                                aAS(7, nType) = aAS(7, nType) - 1
                            ElseIf Trim(fStr("P_METHOD")) = "WEBCCC" Then
                                aAS(9, nType) = aAS(9, nType) - 1
                            Else
                                aAS(8, nType) = aAS(8, nType) - 1
                            End If
                            If IsBlank(fStr("S_DONORID")) Then
                                aAS(6, nType) = aAS(6, nType) - 1
                            Else
                                aAS(5, nType) = aAS(5, nType) - 1
                            End If
                            If Trim(fStr("S_PAY_TYPE")) = "FOC" Then
                                aAS(10, nType) = aAS(10, nType) - 1
                            End If
                        End If
                    End If
                End If
                Call d4skip(data("subscrip"), 1)
            Loop
            
            ' update database
            Set rs = sCnn.OpenRecordset("select as_id from active_subs where as_pub='" + Trim(fStr("PUB_CODE")) + "' and as_issue='" + cIssue + "' and as_type='P' and as_mailing='N'")
            If rs.RecordCount = 0 Then
                ' add record
                cSql = "insert into active_subs (as_batch_id,as_mailing,as_pub,as_issue,as_type) values ('000001','N','" + Trim(fStr("PUB_CODE")) + "','" + cIssue + "','P')"
                sCnn.Execute (cSql)
            End If
            ' update record with values
            cSql = "update active_subs set as_count=" + Str(aAS(7, 0) + aAS(8, 0) + aAS(9, 0)) + ",as_uk_count=" + Str(aAS(1, 0)) + ",as_eu_count=" + Str(aAS(2, 0)) + ",as_us_count=" + Str(aAS(3, 0)) + ",as_row_count=" + Str(aAS(4, 0)) + ",as_gift_count=" + Str(aAS(5, 0)) + ",as_direct_count=" + Str(aAS(6, 0)) + ",as_dd_count=" + Str(aAS(7, 0)) + ",as_non_dd_count=" + Str(aAS(8, 0)) + ",as_ccc_count=" + Str(aAS(9, 0)) + ",as_foc_count=" + Str(aAS(10, 0))
            cSql = cSql + " where as_pub='" + Trim(fStr("PUB_CODE")) + "' and as_issue='" + cIssue + "' and as_type='P' and as_mailing='N'"
            sCnn.Execute (cSql)
            
            Set rs = sCnn.OpenRecordset("select as_id from active_subs where as_pub='" + Trim(fStr("PUB_CODE")) + "' and as_issue='" + cIssue + "' and as_type='D' and as_mailing='N'")
            If rs.RecordCount = 0 Then
                ' add record
                cSql = "insert into active_subs (as_batch_id,as_mailing,as_pub,as_issue,as_type) values ('000001','N','" + Trim(fStr("PUB_CODE")) + "','" + cIssue + "','D')"
                sCnn.Execute (cSql)
            End If
            ' update record with values
            cSql = "update active_subs set as_count=" + Str(aAS(7, 1) + aAS(8, 1) + aAS(9, 1)) + ",as_uk_count=" + Str(aAS(1, 1)) + ",as_eu_count=" + Str(aAS(2, 1)) + ",as_us_count=" + Str(aAS(3, 1)) + ",as_row_count=" + Str(aAS(4, 1)) + ",as_gift_count=" + Str(aAS(5, 1)) + ",as_direct_count=" + Str(aAS(6, 1)) + ",as_dd_count=" + Str(aAS(7, 1)) + ",as_non_dd_count=" + Str(aAS(8, 1)) + ",as_ccc_count=" + Str(aAS(9, 1)) + ",as_foc_count=" + Str(aAS(10, 1))
            cSql = cSql + " where as_pub='" + Trim(fStr("PUB_CODE")) + "' and as_issue='" + cIssue + "' and as_type='D' and as_mailing='N'"
            sCnn.Execute (cSql)
            
            Set rs = sCnn.OpenRecordset("select as_id from active_subs where as_pub='" + Trim(fStr("PUB_CODE")) + "' and as_issue='" + cIssue + "' and as_type='B' and as_mailing='N'")
            If rs.RecordCount = 0 Then
                ' add record
                cSql = "insert into active_subs (as_batch_id,as_mailing,as_pub,as_issue,as_type) values ('000001','N','" + Trim(fStr("PUB_CODE")) + "','" + cIssue + "','B')"
                sCnn.Execute (cSql)
            End If
            ' update record with values
            cSql = "update active_subs set as_count=" + Str(aAS(7, 2) + aAS(8, 2) + aAS(9, 2)) + ",as_uk_count=" + Str(aAS(1, 2)) + ",as_eu_count=" + Str(aAS(2, 2)) + ",as_us_count=" + Str(aAS(3, 2)) + ",as_row_count=" + Str(aAS(4, 2)) + ",as_gift_count=" + Str(aAS(5, 2)) + ",as_direct_count=" + Str(aAS(6, 2)) + ",as_dd_count=" + Str(aAS(7, 2)) + ",as_non_dd_count=" + Str(aAS(8, 2)) + ",as_ccc_count=" + Str(aAS(9, 2)) + ",as_foc_count=" + Str(aAS(10, 2))
            cSql = cSql + " where as_pub='" + Trim(fStr("PUB_CODE")) + "' and as_issue='" + cIssue + "' and as_type='B' and as_mailing='N'"
            sCnn.Execute (cSql)
            
            Call d4skip(data("publish"), 1)
        Loop
        sCnn.CommitTrans
        Print #nLogFile, "End subs since last mailing -" + Format(Now, "YYMMDD hh:mm:ss")
        
        ' new subs source code
        Dim aNSS(7) As Variant
        ' 0 source code
        ' 1 direct
        ' 2 gift
        ' 3 uk
        ' 4 os
        ' 5 dd count
        ' 6 non dd/ccc count
        ' 7 ccc count
        sCnn.BeginTrans
        i = 0
        ' find last sub ID included in report data
        Call d4tagSelect(data("payment"), data("tP_ID"))
        'Call d4tagSelect(data("subscrip"), data("tS_ID"))
        Call d4tagSelect(data("subscrip"), data("tS_CREATED"))
        'Call d4seek(data("subscrip"), StrZero(Val(cLastNewSubSourceID), 6))
        'Get the next day - SS - we shouldn't do this here as we will then be moving the date forward every time its run eg on 29/08/23, last date in init file is 20230901 so will never be found
        ' we could increment the day when we've finished & before storing if any new subs have been found but that's not perfect as we could run multiple times in a day & any subs after we run won't get found
        ' way forward is to use the  data & ID together (will be a problem when sub ID next wraps but that won't be for a while)
        ' above won't work as tS_CREATED isn't a composite index so S_ID not in order
        ' solution is to maintain a list of S_IDs already processed for each day & reset each time we move the date on
        'cLastNewSubDate = Format(DateAdd("d", 1, CDate(Right(cLastNewSubDate, 2) & "/" & Mid(cLastNewSubDate, 5, 2) & "/" & Left(cLastNewSubDate, 4))), "YYYYMMDD")
        Call d4seek(data("subscrip"), cLastNewSubDate)
        'Call d4skip(data("subscrip"), 1)
        Do While d4eof(data("subscrip")) = 0
            If fStr("S_STATUS") <> "CANCEL" And fStr("S_ORDDATE") > "20221129" And InStr(cLastNewSubSourceIDList, fStr("S_ID")) = 0 Then ' check not already processed for this day
                If d4seek(data("customer"), fStr("S_CUSTOMER")) = 0 And d4seek(data("payment"), fStr("S_PAY_ID")) = 0 Then
                    ' update new subs source details
                    For j = 1 To 7
                        aNSS(j) = 0
                    Next j
                    ' source code - new not renewed subs only
                    cNewRenewed = IIf(IsBlank(fStr("S_RENEWOF")), "N", "R")
                    aNSS(0) = fStr("S_SOURCE") + cNewRenewed
                    nSource = i
                    If IsBlank(fStr("S_DONORID")) Then
                        aNSS(1) = 1
                    Else
                        aNSS(2) = 1
                    End If
                    If Trim(fStr("C_COUNTRY")) = "UK" Then
                        aNSS(3) = 1
                    Else
                        aNSS(4) = 1
                    End If
                    If Trim(fStr("P_METHOD")) = "DD" Then
                        aNSS(5) = 1
                    ElseIf Trim(fStr("P_METHOD")) = "WEBCCC" Then
                        aNSS(7) = 1
                    Else
                        aNSS(6) = 1
                    End If
                    ' update database
                    Set rs = sCnn.OpenRecordset("select nss_id from new_subs_source where nss_pub='" + Trim(fStr("S_PUBLICAT")) + "' and nss_source='" + Trim(fStr("S_SOURCE")) + "' and nss_date='" + fStr("S_ORDDATE") + "'")
                    If rs.RecordCount = 0 Then
                        ' add record
                        cSql = "insert into new_subs_source (nss_pub,nss_source,nss_new_renewed,nss_direct_count,nss_gift_count,nss_uk_count,nss_os_count,nss_dd_count,nss_non_dd_count,nss_ccc_count,nss_date) values ("
                        cSql = cSql + "'" + Trim(fStr("S_PUBLICAT")) + "','" + Trim(Left(aNSS(0), 12)) + "','" + Right(aNSS(0), 1) + "'," + CStr(aNSS(1)) + "," + CStr(aNSS(2)) + "," + CStr(aNSS(3)) + "," + CStr(aNSS(4)) + "," + CStr(aNSS(5)) + "," + CStr(aNSS(6)) + "," + CStr(aNSS(7)) + "," + fStr("S_ORDDATE") + ")"
                        sCnn.Execute (cSql)
                    Else
                        ' update record with values
                        cSql = "update new_subs_source set nss_direct_count=nss_direct_count+" + Str(aNSS(1)) + ",nss_gift_count=nss_gift_count+" + Str(aNSS(2)) + ",nss_uk_count=nss_uk_count+" + Str(aNSS(3)) + ",nss_os_count=nss_os_count+" + Str(aNSS(4)) + ",nss_dd_count=nss_dd_count+" + Str(aNSS(5)) + ",nss_non_dd_count=nss_non_dd_count+" + Str(aNSS(6)) + ",nss_ccc_count=nss_ccc_count+" + Str(aNSS(7))
                        cSql = cSql + " where nss_pub='" + Trim(fStr("S_PUBLICAT")) + "' and nss_source='" + Trim(fStr("S_SOURCE")) + "' and nss_new_renewed='" + Right(aNSS(0), 1) + "' and nss_date='" + fStr("S_ORDDATE") + "'"
                        sCnn.Execute (cSql)
                    End If
                End If
            End If
            If fStr("S_CREATED") > cLastNewSubDate Then
                ' we've moved on to another day so set last date & reset sub ID list
                cLastNewSubDate = fStr("S_CREATED")
                cLastNewSubSourceIDList = fStr("S_ID")
            Else
                cLastNewSubSourceIDList = cLastNewSubSourceIDList + "," + fStr("S_ID") ' get latest ID to have been checked
            End If
                
            Label1 = Str(i) + " - " + fStr("S_ID")
            Label1.Refresh
            i = i + 1
            If Round(i / 1000) = i / 1000 Then
                DoEvents
            End If
            Call d4skip(data("subscrip"), 1)
        Loop
        Call cIni.WriteINI(App.Path + "\OakReports.ini", "OakReports", "LastNewSubSourceID", cLastNewSubSourceIDList)
        Call cIni.WriteINI(App.Path + "\OakReports.ini", "OakReports", "LastNewSubDate", cLastNewSubDate)
        sCnn.CommitTrans
                
        Print #nLogFile, "Start mail order -" + Format(Now, "YYMMDD hh:mm:ss")
        ' product sales
        'Dim cOrdDate As String
        'cOrdDate = Format(DateAdd("d", -60, Now), "YYYYMMDD") ' 60 days ago
        
        sTemp.BeginTrans
        i = 0
        ' find last order ID included in report data
        Call d4seek(data("morderh"), StrZero(Val(cLastMailOrderID), 6))
        Call d4skip(data("morderh"), 1)
        Do While d4eof(data("morderh")) = 0
            If fStr("MH_STATUS") <> "CANCEL" And fStr("MH_ORDDATE") > "20161231" Then
                Call d4seek(data("morderl"), fStr("MH_ID"))
                Do While fStr("ML_ORDER") = fStr("MH_ID")
                    nValue = Round((f4int(data("ML_QTY")) * (f4double(data("ML_V1UNIT")) + f4double(data("ML_V2UNIT")))) - f4double(data("ML_DISC")), 2)
                    If fStr("MH_CURR") <> "UKP" Then
                        Call d4seek(data("currency"), fStr("MH_CURR"))
                        nValue = nValue / Val(fStr("CUR_RATE"))
                    End If
                    If nValue > 0 Then
                        ' add each order line
                        'Set rsTemp = sTemp.OpenRecordset("Select mo_product from mailorder where mo_product='" + Trim(fStr("ML_PRODUCT")) + "' and mo_date='" + fStr("MH_ORDDATE") + "'")
                        'If rsTemp.RecordCount = 0 Then
                            sTemp.Execute "Insert into mailorder values('" + Trim(fStr("ML_ORDER")) + "','" + Trim(fStr("ML_PRODUCT")) + "','" + fStr("MH_ORDDATE") + "'," + fStr("ML_QTY") + "," + Str(nValue) + ")"
                        'ElseIf rsTemp.RecordCount > 1 Then
                        'Else
                        '    sTemp.Execute "update mailorder set mo_qty=mo_qty+" + fStr("ML_QTY") + ", mo_value=mo_value+" + Str(nValue) + " where mo_product='" + Trim(fStr("ML_PRODUCT")) + "' and mo_date='" + fStr("MH_ORDDATE") + "'"
                        'End If
                    End If
                    Call d4skip(data("morderl"), 1)
                Loop
            End If
            cLastMailOrderID = fStr("MH_ID")
            Label1 = Str(i) + " - " + fStr("MH_ID")
            Label1.Refresh
            i = i + 1
            If Round(i / 1000) = i / 1000 Then
                DoEvents
            End If
            Call d4skip(data("morderh"), 1)
        Loop
        Call cIni.WriteINI(App.Path + "\OakReports.ini", "OakReports", "LastMailOrderID", StrZero(Val(cLastMailOrderID), 6))
        sTemp.CommitTrans
        
        ' update report db
        sCnn.BeginTrans
        Set rsTemp = sTemp.OpenRecordset("Select * from mailorder")
        If rsTemp.RecordCount > 0 Then
            rsTemp.MoveFirst
            Do While Not rsTemp.EOF
                ' add each order line
                'Set rs = sCnn.OpenRecordset("select * from mail_order where mo_product='" + rsTemp("mo_product") + "' and mo_date='" + rsTemp("mo_date") + "'")
                'If rs.RecordCount = 0 Then
                    ' insert
                    sCnn.Execute "insert into mail_order (mo_order_id,mo_product,mo_date,mo_qty,mo_value) values('" + rsTemp("mo_order_id") + "','" + rsTemp("mo_product") + "','" + rsTemp("mo_date") + "'," + Str(rsTemp("mo_qty")) + "," + Str(rsTemp("mo_value")) + ")"
                'ElseIf Val(rs.Fields("mo_qty")) <> Val(rsTemp("mo_qty")) Or Val(rs.Fields("mo_value")) <> Val(rsTemp("mo_value")) Then
                '    ' Update
                '    sCnn.Execute "update mail_order set mo_qty=" + Str(rsTemp("mo_qty")) + ", mo_value=" + Str(rsTemp("mo_value")) + " where mo_product='" + rsTemp("mo_product") + "' and mo_date='" + rsTemp("mo_date") + "'"
                'End If
                rsTemp.MoveNext
            Loop
        End If
        sCnn.CommitTrans
        
        
        Print #nLogFile, "End mail order -" + Format(Now, "YYMMDD hh:mm:ss")
        
                
        ' update the actual db to be used for reports
        
        ' update report db - pay method
    '    sCnn.BeginTrans
    '    Set rsTemp = sTemp.OpenRecordset("Select * from issuepay")
    '    If rsTemp.RecordCount > 0 Then
    '        rsTemp.MoveFirst
    '        Do While Not rsTemp.EOF
    '            If Val(rsTemp.Fields("ip_count").Value) > 0 Then
    '                ' non zero count
    '                cIssue = Left(rsTemp.Fields("ip_issuepay").Value, 6)
    '                cPayMethod = Mid(rsTemp.Fields("ip_issuepay").Value, 7)
    '                nCount = Val(rsTemp.Fields("ip_count"))
    '                nValue = Val(rsTemp.Fields("ip_value"))
    '                nNewCount = Val(rsTemp.Fields("ip_new_count"))
    '                nNewValue = Val(rsTemp.Fields("ip_new_value"))
    '                nRenCount = Val(rsTemp.Fields("ip_ren_count"))
    '                nRenValue = Val(rsTemp.Fields("ip_ren_value"))
    '                cPub = rsTemp.Fields("ip_pub").Value
    '                Set rs = sCnn.OpenRecordset("select * from activesubspaymethod where aspm_paymethod='" + cPayMethod + "' and aspm_pub='" + cPub + "' and aspm_issue='" + cIssue + "'")
    '                If rs.RecordCount = 0 Then
    '                    ' insert
    '                    sCnn.Execute "insert into activesubspaymethod (aspm_paymethod,aspm_pub,aspm_count,aspm_value,aspm_new_count,aspm_new_value,aspm_ren_count,aspm_ren_value, aspm_paydesc, aspm_issue) values('" + cPayMethod + "','" + Trim(cPub) + "'," + Str(nCount) + "," + Str(nValue) + "," + Str(nNewCount) + "," + Str(nNewValue) + "," + Str(nRenCount) + "," + Str(nRenValue) + ",'" + GetPayDesc(cPayMethod) + "','" + cIssue + "')"
    '                ElseIf Val(rs.Fields("aspm_count")) <> nCount Or Val(rs.Fields("aspm_new_count")) <> nNewCount Then
    '                    ' Update
    '                    sCnn.Execute "update activesubspaymethod set aspm_count=" + Str(nCount) + ", aspm_value=" + Str(nValue) + ",aspm_new_count=" + Str(nNewCount) + ", aspm_new_value=" + Str(nNewValue) + ",aspm_ren_count=" + Str(nRenCount) + ", aspm_ren_value=" + Str(nRenValue) + ", aspm_paydesc='" + GetPayDesc(cPayMethod) + "' where aspm_paymethod='" + cPayMethod + "' and aspm_pub='" + cPub + "' and aspm_issue='" + cIssue + "'"
    '                End If
    '            End If
    '            rsTemp.MoveNext
    '        Loop
    '    End If
    '    sCnn.CommitTrans
    '    clsLogAutoProcess.cProcessNotes = clsLogAutoProcess.cProcessNotes + "SubsByPayMethod updated " + vbCrLf
    
        ' update report db - issue renewal
        Print #nLogFile, "Start issuerenewal update -" + Format(Now, "YYMMDD hh:mm:ss")
        i = 0
        sCnn.BeginTrans
        Set rsTemp = sTemp.OpenRecordset("Select * from issuerenewal")
        If rsTemp.RecordCount > 0 Then
            rsTemp.MoveFirst
            Do While Not rsTemp.EOF
                If Val(rsTemp.Fields("ir_count").Value) > 0 Then
                    ' non zero count
                    cIssue = Left(rsTemp.Fields("ir_issue").Value, 6)
                    cPub = rsTemp.Fields("ir_pub").Value
                    cRegion = rsTemp("ir_region")
                    cSource = rsTemp("ir_source")
                    aRenewal(0) = Val(rsTemp.Fields("ir_count"))
                    aRenewal(1) = Val(rsTemp.Fields("ir_non_cont_count"))
                    aRenewal(2) = Val(rsTemp.Fields("ir_non_cont_expire"))
                    aRenewal(3) = Val(rsTemp.Fields("ir_non_cont_renew1"))
                    aRenewal(4) = Val(rsTemp.Fields("ir_non_cont_renew2"))
                    aRenewal(5) = Val(rsTemp.Fields("ir_cont_count"))
                    aRenewal(6) = Val(rsTemp.Fields("ir_cont_expire"))
                    aRenewal(7) = Val(rsTemp.Fields("ir_cont_renew1"))
                    aRenewal(11) = Val(rsTemp.Fields("ir_subs_starting"))
                    If aRenewal(2) > 0 Then
                        aRenewal(8) = Round((aRenewal(3) + aRenewal(4)) / aRenewal(2) * 100, 2) ' renewal rate non continuous
                    Else
                        aRenewal(8) = 0
                    End If
                    If aRenewal(6) > 0 Then
                        aRenewal(9) = Round(aRenewal(7) / aRenewal(6) * 100, 2) ' renewal rate continuous
                    Else
                        aRenewal(9) = 0
                    End If
                    aRenewal(10) = Round((aRenewal(1) / aRenewal(0)) * aRenewal(8), 2) + Round((aRenewal(5) / aRenewal(0)) * aRenewal(9), 2)
                    Set rs = sCnn.OpenRecordset("select * from issuerenewal where ir_pub='" + cPub + "' and ir_issue='" + cIssue + "' and ir_region='" + cRegion + "' and ir_source='" + cSource + "'")
                    If rs.RecordCount = 0 Then
                        ' insert
                        sCnn.Execute "insert into issuerenewal (ir_issue,ir_pub,ir_count,ir_non_cont_count,ir_non_cont_expire,ir_non_cont_renew1,ir_non_cont_renew2,ir_non_cont_renew,ir_cont_count,ir_cont_expire,ir_cont_renew1,ir_cont_renew,ir_renewal_rate,ir_subs_starting,ir_region,ir_source) values('" + cIssue + "','" + cPub + "'," + Str(aRenewal(0)) + "," + Str(aRenewal(1)) + "," + Str(aRenewal(2)) + "," + Str(aRenewal(3)) + "," + Str(aRenewal(4)) + "," + Str(aRenewal(8)) + "," + Str(aRenewal(5)) + "," + Str(aRenewal(6)) + "," + Str(aRenewal(7)) + "," + Str(aRenewal(9)) + "," + Str(aRenewal(10)) + "," + Str(aRenewal(11)) + ",'" + cRegion + "','" + cSource + "')"
                    ElseIf Val(rs.Fields("ir_count")) <> nCount Then
                        ' Update
                        sCnn.Execute "update issuerenewal set ir_count=" + Str(aRenewal(0)) + ", ir_non_cont_count=" + Str(aRenewal(1)) + ", ir_non_cont_expire=" + Str(aRenewal(2)) + ",ir_non_cont_renew1=" + Str(aRenewal(3)) + ", ir_non_cont_renew2=" + Str(aRenewal(4)) + ",ir_non_cont_renew=" + Str(aRenewal(8)) + ", ir_cont_count=" + Str(aRenewal(5)) + ", ir_cont_expire=" + Str(aRenewal(6)) + ", ir_cont_renew1=" + Str(aRenewal(7)) + ", ir_cont_renew=" + Str(aRenewal(9)) + ",ir_renewal_rate=" + Str(aRenewal(10)) + ",ir_subs_starting=" + Str(aRenewal(11)) + " where ir_pub='" + cPub + "' and ir_issue='" + cIssue + "' and ir_region='" + cRegion + "' and ir_source='" + cSource + "'"
                    End If
                End If
                Label1 = Str(i) + " - " + cPub + " " + cIssue
                Label1.Refresh
                i = i + 1
                If Round(i / 1000) = i / 1000 Then
                    DoEvents
                End If
                rsTemp.MoveNext
            Loop
        End If
        sCnn.CommitTrans
        Print #nLogFile, "End issuerenewal update -" + Format(Now, "YYMMDD hh:mm:ss")
        clsLogAutoProcess.cProcessNotes = clsLogAutoProcess.cProcessNotes + "IssueRenewal updated " + Format(Now, "YYMMDD hh:mm:ss") + vbCrLf

        ' update issue adjustments
        sTemp.BeginTrans
        d4top (data("issueadj"))
        Do While d4eof(data("issueadj")) = 0
            Set rsTemp = sTemp.OpenRecordset("select ia_pub from issue_adj where ia_pub='" + fStr("ISSA_PUB") + "' and ia_issue='" + fStr("ISSA_ISSUE") + "' and ia_reason='" + fStr("ISSA_REAS") + "'")
            If rsTemp.RecordCount = 0 Then
                ' insert
                sTemp.Execute "insert into issue_adj (ia_pub,ia_issue,ia_reason,ia_qty)" + " values ('" + fStr("ISSA_PUB") + "','" + fStr("ISSA_ISSUE") + "','" + fStr("ISSA_REAS") + "'," + fStr("ISSA_QTY") + ")"
            Else
                ' Update
                sTemp.Execute "update issue_adj set ia_qty=ia_qty+" + Str(Val(fStr("ISSA_QTY"))) + " where ia_pub='" + fStr("ISSA_PUB") + "' and ia_issue='" + fStr("ISSA_ISSUE") + "' and ia_reason='" + fStr("ISSA_REAS") + "'"
            End If
            
            Call d4skip(data("issueadj"), 1)
        Loop
        sTemp.CommitTrans
        sCnn.BeginTrans
        sCnn.Execute ("update issue_adj set ia_qty=0")
        sCnn.CommitTrans
        sCnn.BeginTrans
        Set rsTemp = sTemp.OpenRecordset("Select * from issue_adj")
        If rsTemp.RecordCount > 0 Then
            rsTemp.MoveFirst
            Do While Not rsTemp.EOF
                Set rs = sCnn.OpenRecordset("select ia_id from issue_adj where ia_pub='" + rsTemp("ia_pub") + "' and ia_issue='" + rsTemp("ia_issue") + "' and ia_reason='" + rsTemp("ia_reason") + "'")
                If rs.RecordCount = 0 Then
                    ' insert
                    sCnn.Execute "insert into issue_adj (ia_pub,ia_issue,ia_reason,ia_qty) values ('" + rsTemp("ia_pub") + "','" + rsTemp("ia_issue") + "','" + rsTemp("ia_reason") + "'," + Str(rsTemp("ia_qty")) + ")"
                Else
                    ' Update
                    sCnn.Execute "update issue_adj set ia_qty=ia_qty+" + Str(rsTemp("ia_qty")) + " where ia_pub='" + rsTemp("ia_pub") + "' and ia_issue='" + rsTemp("ia_issue") + "' and ia_reason='" + rsTemp("ia_reason") + "'"
                End If
                rsTemp.MoveNext
            Loop
        End If
        sCnn.CommitTrans
    
        ' update issues_sent
    '    sCnn.BeginTrans
    '    Set rsTemp = sTemp.OpenRecordset("Select * from issuesent")
    '    If rsTemp.RecordCount > 0 Then
    '        rsTemp.MoveFirst
    '        Do While Not rsTemp.EOF
    '            If Val(rsTemp.Fields("is_resent")) > 0 Then
    '                ' non zero count
    '                cIssue = rsTemp.Fields("is_issue")
    '                cPub = rsTemp.Fields("is_pub")
    '                cCountry = rsTemp.Fields("is_country")
    '                If d4seek(data("country"), cCountry) = 0 Then
    '                    cCountryDesc = fStr("CTRY_DESC")
    '                Else
    '                    cCountryDesc = cCountry
    '                End If
    '                nCount = Val(rsTemp.Fields("is_resent"))
    '                Set rs = sCnn.OpenRecordset("select * from issues_sent where is_country='" + cCountry + "' and is_pub='" + cPub + "' and is_issue='" + cIssue + "'")
    '                If rs.RecordCount = 0 Then
    '                    ' insert
    '                    sCnn.Execute "insert into issues_sent (is_pub,is_issue,is_country,is_countrydesc,is_resent) values('" + Trim(cPub) + "','" + Trim(cIssue) + "','" + cCountry + "','" + cCountryDesc + "'," + Str(nCount) + ")"
    '                ElseIf Val(rs.Fields("is_resent")) <> nCount Then
    '                    ' Update
    '                    sCnn.Execute "update issues_sent set is_resent=" + Str(nCount) + "  where is_country='" + cCountry + "' and is_pub='" + cPub + "' and is_issue='" + cIssue + "'"
    '                End If
    '            End If
    '            rsTemp.MoveNext
    '        Loop
    '    End If
    '    sCnn.CommitTrans
    '    clsLogAutoProcess.cProcessNotes = clsLogAutoProcess.cProcessNotes + "IssuesSent updated " + vbCrLf
        
        ' update agent_sales
    '    sCnn.BeginTrans
    '    Set rsTemp = sTemp.OpenRecordset("Select * from agentsales")
    '    If rsTemp.RecordCount > 0 Then
    '        rsTemp.MoveFirst
    '        Do While Not rsTemp.EOF
    '            If Val(rsTemp.Fields("as_count")) > 0 Then
    '                ' non zero count
    '                cMonth = rsTemp.Fields("as_month")
    '                cPub = rsTemp.Fields("as_pub")
    '                cAgent = rsTemp.Fields("as_agent")
    '                cSubType = rsTemp.Fields("as_subtype")
    '                nCount = Val(rsTemp.Fields("as_count"))
    '                nValue = Val(rsTemp.Fields("as_value"))
    '                Set rs = sCnn.OpenRecordset("select * from agent_sales where as_agent='" + cAgent + "' and as_pub='" + cPub + "' and as_month='" + cMonth + "' and as_subtype='" + cSubType + "'")
    '                If rs.RecordCount = 0 Then
    '                    ' insert
    '                    sCnn.Execute "insert into agent_sales (as_agent, as_pub,as_month,as_subtype,as_count,as_value) values('" + cAgent + "','" + Trim(cPub) + "','" + cMonth + "','" + cSubType + "'," + Str(nCount) + "," + Str(nValue) + ")"
    '                ElseIf Val(rs.Fields("as_count")) <> nCount Then
    '                    ' Update
    '                    sCnn.Execute "update agent_sent set as_count=" + Str(nCount) + ", as_value=" + Str(nValue) + " where as_agent='" + cAgent + "' and as_pub='" + cPub + "' and as_month='" + cMonth + "' and as_subtype='" + cSubType + "'"
    '                End If
    '            End If
    '
    '            rsTemp.MoveNext
    '        Loop
    '    End If
    '    sCnn.CommitTrans
    '    clsLogAutoProcess.cProcessNotes = clsLogAutoProcess.cProcessNotes + "AgentSales updated " + vbCrLf
        
        ' update dd_count
    '    sCnn.BeginTrans
    '    sCnn.Execute "delete from dd_count"
    '    sCnn.CommitTrans
    '    sCnn.BeginTrans
    '    Set rsTemp = sTemp.OpenRecordset("Select * from dd_count")
    '    If rsTemp.RecordCount > 0 Then
    '        rsTemp.MoveFirst
    '        Do While Not rsTemp.EOF
    '            sCnn.Execute "Insert into dd_count values('" + rsTemp("dd_pub") + "','" + rsTemp("dd_offer") + "','" + rsTemp("dd_status") + "'," + Str(rsTemp("dd_pay_count")) + "," + Str(rsTemp("dd_sub_count")) + ",'')"
    '            rsTemp.MoveNext
    '        Loop
    '    End If
    '    sCnn.CommitTrans
    '    clsLogAutoProcess.cProcessNotes = clsLogAutoProcess.cProcessNotes + "AgentSales updated " + vbCrLf
        
        sCnn.BeginTrans
        d4top (data("publish"))
        Do While d4eof(data("publish")) = 0
            If InStr(fStr("PUB_DESC"), "CLOSED") = 0 Then
                Set rs = sCnn.OpenRecordset("select * from publication where pub_code='" + fStr("PUB_CODE") + "'")
                If rs.RecordCount = 0 Then
                    ' insert
                    sCnn.Execute "insert into publication (pub_code, pub_desc) values('" + fStr("PUB_CODE") + "','" + NoQuotes(fStr("PUB_DESC")) + "')"
                End If
            Else
                ' delete
                sCnn.Execute "delete from publication where pub_code='" + fStr("PUB_CODE") + "'"
            End If
            Call d4skip(data("publish"), 1)
        Loop
        d4top (data("issue"))
        Do While d4eof(data("issue")) = 0
            If fStr("ISS_MAIL") >= "20150401" Then
                Set rs = sCnn.OpenRecordset("select * from issue where iss_pub='" + fStr("ISS_PUB") + "' and iss_number='" + fStr("ISS_NUMBER") + "'")
                If rs.RecordCount = 0 Then
                    ' insert
                    sCnn.Execute "insert into issue (iss_pub, iss_number,iss_desc) values('" + fStr("ISS_PUB") + "','" + fStr("ISS_NUMBER") + "','" + Replace(fStr("ISS_DESC"), "'", "") + "')"
                Else
                    ' update issue as description could have changed
                    sCnn.Execute "update issue set iss_desc='" + Replace(fStr("ISS_DESC"), "'", "") + "' where iss_pub='" + fStr("ISS_PUB") + "' and iss_number='" + fStr("ISS_NUMBER") + "'"
                End If
            End If
            Call d4skip(data("issue"), 1)
        Loop
        d4top (data("source"))
        Do While d4eof(data("source")) = 0
            Set rs = sCnn.OpenRecordset("select * from source where srce_code='" + fStr("SRCE_CODE") + "'")
            If rs.RecordCount = 0 Then
                ' insert
                sCnn.Execute "insert into source (srce_code, srce_desc) values('" + fStr("SRCE_CODE") + "','" + NoQuotes(fStr("SRCE_DESC")) + "')"
            End If
            Call d4skip(data("source"), 1)
        Loop
        d4top (data("batch"))
        Do While d4eof(data("batch")) = 0
            If fStr("BAT_TYPE") = "M" Then
                Set rs = sCnn.OpenRecordset("select * from batch where bat_batch='" + fStr("BAT_BATCH") + "'")
                If rs.RecordCount = 0 Then
                    ' insert
                    sCnn.Execute "insert into batch (bat_batch, bat_date) values('" + fStr("BAT_BATCH") + "','" + fStr("BAT_DATE") + "')"
                End If
            End If
            Call d4skip(data("batch"), 1)
        Loop
        d4top (data("product"))
        Do While d4eof(data("product")) = 0
            Set rs = sCnn.OpenRecordset("select * from product where pr_code='" + Trim(fStr("PR_CODE")) + "'")
            If rs.RecordCount = 0 Then
                ' insert
                sCnn.Execute "insert into product (pr_code, pr_desc, pr_category,pr_webcategory4,pr_webcategory16) values('" + Trim(fStr("PR_CODE")) + "','" + NoQuotes(fStr("PR_DESC")) + "','" + Trim(fStr("PR_CAT")) + "','" + Trim(fStr("PR_WEBCAT4")) + "','" + Trim(fStr("PR_WEBCT16")) + "')"
            ElseIf Trim(NoQuotes(fStr("PR_DESC"))) <> Trim(rs.Fields("pr_desc")) Or Trim(fStr("PR_CAT")) <> Trim(rs.Fields("pr_category")) Or Trim(fStr("PR_WEBCAT4")) <> Trim(rs.Fields("pr_webcategory4")) Or Trim(fStr("PR_WEBCT16")) <> Trim(rs.Fields("pr_webcategory16")) Then
                'update
                sCnn.Execute "update product set pr_desc='" + NoQuotes(fStr("PR_DESC")) + "',pr_category='" + Trim(fStr("PR_CAT")) + "',pr_webcategory4='" + Trim(fStr("PR_WEBCAT4")) + "',pr_webcategory16='" + Trim(fStr("PR_WEBCT16")) + "' where pr_code='" + Trim(fStr("PR_CODE")) + "'"
            End If
            Label1 = "Product - " + Trim(fStr("PR_CODE"))
            Label1.Refresh
            Call d4skip(data("product"), 1)
        Loop
        sCnn.CommitTrans
        
        
        Set rs = Nothing
        Set rsTemp = Nothing
        Set sCnn = Nothing
        Set sTemp = Nothing
        
        ' zip and ftp
        'If FtpUpload Then
        If AwsS3Upload Then
            Print #nFile, Format(Now, "DD/MM/YYYY - hh:mm:ss") + " report summary data ftped"
            clsLogAutoProcess.cProcessNotes = clsLogAutoProcess.cProcessNotes + "db uploaded to web "
            clsLogAutoProcess.cProcessResult = "Success"
        Else
            Print #nFile, Format(Now, "DD/MM/YYYY - hh:mm:ss") + " upload ERROR"
            clsLogAutoProcess.cProcessNotes = clsLogAutoProcess.cProcessNotes + "db NOT uploaded to web "
            clsLogAutoProcess.cProcessResult = "FAILURE"
        End If
               
        clsLogAutoProcess.LogAutoProcessFinish
               

        ' close data
        code4close (cb)
        code4initUndo (cb)
        
        
        Print #nFile, Format(Now, "DD/MM/YYYY - hh:mm:ss") + " finished"
        Close #nFile
        Close #nRenewalFile
        Close #nLogFile
    End If
    Unload Me
    
End Sub

Private Function GetPayDesc(cPayMethod As String)
    If d4seek(data("pay_meth"), cPayMethod) = 0 Then
        GetPayDesc = Trim(fStr("PM_DESC"))
    ElseIf cPayMethod = "UNKNWN" Then
        GetPayDesc = "Unknown payment method (imported data)"
    Else
        GetPayDesc = cPayMethod
    End If
End Function
Public Sub FormGradient(oObject As Object, nRGB As Long, nStep As Integer, Optional nLines As Integer)
    If nLines = 0 Then nLines = 63
    Dim nFillTop As Long, nFillLeft As Long, nFillRight As Long, nFillBottom As Long
    Dim nHeight As Integer, nRed As Long, nGreen As Long, nBlue As Long, i As Integer
    nBlue = Int(nRGB / 65536)
    nGreen = Int((nRGB - (65536 * nBlue)) / 256)
    nRed = Int(nRGB - (65536 * nBlue) - (256 * nGreen))
    'This will create 63 steps in the gradient. This looks smooth on 16-bit and 24-bit color.
    nHeight = (oObject.Height / nLines)
    'This tells it whether to start on the top or the bottom and adjusts variables accordingly.
    nFillTop = 0
    nFillLeft = 0
    nFillRight = oObject.Width
    nFillBottom = nFillTop + nHeight
    For i = 1 To nLines
        'This draws the colored bar.
        oObject.Line (nFillLeft, nFillTop)-(nFillRight, nFillBottom), RGB(nRed, nGreen, nBlue), BF
        'This decreases the RGB values to darken the color.
        nRed = nRed - nStep
        nGreen = nGreen - nStep
        nBlue = nBlue - nStep
        'This prevents the RGB values from becoming negative, which causes a runtime error.
        If nRed <= 0 Then nRed = 0
        If nGreen <= 0 Then nGreen = 0
        If nBlue <= 0 Then nBlue = 0
        nFillTop = nFillTop + nHeight
        nFillBottom = nFillTop + nHeight
    Next i
End Sub
Private Function fStr(cData As String)
    fStr = f4str(data(cData))
End Function
Private Function IsBlank(cString As String)
    IsBlank = (Len(Trim(cString)) = 0)
End Function
Private Function StrZero(n As Long, nLen As Integer)
    StrZero = Right(String(nLen, "0") + Trim(Str(n)), nLen)
End Function
Private Function PadStr(cString As String, nLen As Integer)
    PadStr = Left(cString + String(nLen, " "), nLen)
End Function
Private Function NoQuotes(ByVal cString As String)
    Dim i As Integer, cChar As String
    NoQuotes = ""
    cString = Trim(cString)
    For i = 1 To Len(cString)
        cChar = Mid(cString, i, 1)
        If cChar = Chr(34) Or cChar = "'" Then
        Else
            NoQuotes = NoQuotes + cChar
        End If
    Next i
End Function
Private Function FtpUpload()
    Dim cSFTP As New clsSFTP, lOK As Boolean
    lOK = False
    If cSFTP.Init("kelsey.oakreports.co.uk", cFTPUserName, "", "/var/www/oaksoftware/oakreports/generated/data/") Then
        If cSFTP.SFTPUpload(App.Path + "\KelseyOakReports.db", "KelseyOakReports.db") Then
            lOK = True
        End If
    End If
    FtpUpload = lOK
End Function
Private Function AwsS3Upload()
    Dim http As New ChilkatHttp, nResult As Long
    AwsS3Upload = False
    nResult = http.UnlockComponent("OAKSFT.CB1022020_ddzCMuHRne55")
    If (nResult <> 1) Then
        MsgBox http.LastErrorText
    Else
        http.AwsAccessKey = cS3AccessKey
        http.AwsSecretKey = cS3SecretKey
        http.AwsRegion = "eu-west-1"
        nResult = http.S3_UploadFile(App.Path + "\KelseyOakReports.db", "application/vnd.sqlite3", cS3BucketName, "/oaksoftware/oakreports/db/KelseyOakReports.db")
        If (nResult = 1) Then
            AwsS3Upload = True
        Else
            Dim nLogFile As Integer
            nLogFile = FreeFile
            Open App.Path + "\OakReportsLog.txt" For Append As #nLogFile
            Print #nLogFile, "_________________________"
            Print #nLogFile, Format(Now, "DD/MM/YYYY - hh:mm:ss") + http.LastErrorText
            Close #nLogFile
        End If
    End If
End Function
Private Sub GetLatestIssues(cPub As String)
    Dim i As Integer, j As Integer, cTemp As String
    For i = 0 To 50
        For j = 0 To 6
            aIssue(i, j) = ""
        Next j
    Next i
    If d4seek(data("issue"), cPub) = 0 Then
        cFirstIssue = ""
        Do While fStr("ISS_PUB") = cPub And d4eof(data("issue")) = 0
            If Val(fStr("ISS_NUMBER")) > 0 Then
                If (fStr("ISS_MAIL") >= "20150401" Or (InStr("~CFD~CMT~FTC~FTF~MMG~TLP~TVX~VRS", Trim(fStr("ISS_PUB"))) > 0 And fStr("ISS_MAIL") >= "20141201")) And IsBlank(cFirstIssue) Then
                    cFirstIssue = Trim(fStr("ISS_NUMBER"))
                End If
                If IsBlank(fStr("ISS_MAILUK")) Then
                    ' gone past last issue
                    Call d4skip(data("issue"), -2) ' back 1 to get to last sent (aissue(0)) & another 1 to get the previous issue to that
                    For i = 1 To IIf(nIssuesPerYear * 3 > 36, 36, nIssuesPerYear * 3)
                        aIssue(i, 0) = fStr("ISS_NUMBER")
                        aIssue(i, 1) = fStr("ISS_MAIL")
                        cTemp = GetIssueX(cPub, fStr("ISS_NUMBER"), -nIssuesPerYear)
                        aIssue(i, 2) = Left(cTemp, 6)
                        aIssue(i, 3) = Mid(cTemp, 7)
                        cTemp = GetIssueX(cPub, fStr("ISS_NUMBER"), nIssuesPerYear)
                        aIssue(i, 4) = Left(cTemp, 6)
                        aIssue(i, 5) = Mid(cTemp, 7)
                        aIssue(i, 6) = aIssue(0, 0)
                        Call d4skip(data("issue"), -1)
                    Next i
                    d4bottom (data("issue"))
                Else
                    aIssue(0, 0) = fStr("ISS_NUMBER")
                    aIssue(0, 1) = fStr("ISS_MAIL")
                    cTemp = GetIssueX(cPub, fStr("ISS_NUMBER"), -nIssuesPerYear)
                    aIssue(0, 2) = Left(cTemp, 6)
                    aIssue(0, 3) = Mid(cTemp, 7)
                    cTemp = GetIssueX(cPub, fStr("ISS_NUMBER"), nIssuesPerYear)
                    aIssue(0, 4) = Left(cTemp, 6)
                    aIssue(0, 5) = Mid(cTemp, 7)
                    aIssue(0, 6) = aIssue(0, 0)
                End If
            End If
            Call d4skip(data("issue"), 1)
        Loop
    End If
End Sub
Private Function GetLatestIssue(cPub As String)
    Dim cTemp As String
    cTemp = ""
    If d4seek(data("issue"), cPub) = 0 Then
        Do While fStr("ISS_PUB") = cPub And d4eof(data("issue")) = 0
            If Val(fStr("ISS_NUMBER")) > 0 Then
                If IsBlank(fStr("ISS_MAILUK")) Then
                    ' gone past last issue
                    d4bottom (data("issue"))
                Else
                    cTemp = fStr("ISS_NUMBER")
                End If
            End If
            Call d4skip(data("issue"), 1)
        Loop
    End If
    GetLatestIssue = cTemp
End Function
Private Function GetIssueX(cPub As String, cIssue As String, nCount)
    Dim nCurRec As Long, i As Integer
    GetIssueX = fStr("ISS_NUMBER") + fStr("ISS_MAIL")
    nCurRec = d4recNo(data("issue"))
    Call d4skip(data("issue"), nCount)
    If fStr("ISS_PUB") = cPub Then
        GetIssueX = fStr("ISS_NUMBER") + fStr("ISS_MAIL")
    End If
    Call d4go(data("issue"), nCurRec)
End Function
Private Function GetAgentSource()
    Dim cTemp As String
    cTemp = ""
    d4top (data("source"))
    Do While d4eof(data("source")) = 0
        If Trim(fStr("SRCE_TYPE")) = "AGENT" Then
            cTemp = cTemp + fStr("SRCE_CODE") + "~"
        End If
        Call d4skip(data("source"), 1)
    Loop
    GetAgentSource = cTemp
End Function
Public Function KeyExists(ByVal oCol As Collection, ByVal vKey As Variant) As Boolean
    On Error Resume Next
    oCol.Item vKey
    KeyExists = (Err.Number = 0)
    Err.Clear

End Function
