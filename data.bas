Attribute VB_Name = "data"
Option Explicit

Public Sub DbfOpen(ByVal aDbfs, cb As Long, data As Collection, cPath As String, lTag As Boolean, lField As Boolean, lOpenMdx As Boolean)
On Error GoTo ERR_HANDLER
    Dim i As Integer, j As Integer, X As Long, dData As Long, cIni As New clsIni
    Dim cIniString As String, lClientServer As Boolean, cCSPort As String, cIPAddress As String, cCSUser As String
    ' check for C/S
    lClientServer = False
    If Dir(App.Path + "\subscribe.ini") <> "" Then
        cIniString = cIni.sGetINI(App.Path + "\subscribe.ini", "ClientServer", "Client Server", "")
        If UCase(cIniString) = "YES" Then
            lClientServer = True
            cCSPort = cIni.sGetINI(App.Path + "\subscribe.ini", "ClientServer", "Port", "")
            cIPAddress = cIni.sGetINI(App.Path + "\subscribe.ini", "ClientServer", "IP_Address", "")
            cCSUser = cIni.sGetINI(App.Path + "\subscribe.ini", "ClientServer", "User", "")
            ' connect to server
            Dim rc As Integer
            rc = code4connect(cb, cIPAddress, cCSPort, cCSUser, "", "")
            If (rc <> r4success) Then
                MsgBox "Data Access Error - Could not connect to the server"
            End If
        End If
    End If
    
    Call code4lockAttempts(cb, 5)
    For i = 0 To UBound(aDbfs)
        ' open database
        Call code4errOpen(cb, 0) ' disable open error
        Call code4autoOpen(cb, IIf(lOpenMdx Or aDbfs(i) = "subddoak", 1, 0))
        If lClientServer Then
            dData = d4open(cb, aDbfs(i) + ".dbf")
        Else
            dData = d4open(cb, cPath + "\" + aDbfs(i) + ".dbf")
        End If
        If dData > 0 Then
            data.Add dData, aDbfs(i)
            If lTag Then
                ' tags
                X = d4tagNext(data(aDbfs(i)), 0&) 'Get first tag
                If X > 0 Then ' if tag exists
                    data.Add X, "t" + t4Alias(X)
                    Call d4tagSelect(data(aDbfs(i)), data("t" + t4Alias(X))) ' make first tag selected
                    d4top (data(aDbfs(i)))
                    Do Until X = 0
                        X = d4tagNext(data(aDbfs(i)), X)   'Look for next tag
                        If X > 0 Then
                            data.Add X, "t" + t4Alias(X)
                        End If
                    Loop
                Else
                    d4top (data(aDbfs(i)))
                End If
                If lField Then
                    ' get fields as well as tags
                    For j = 1 To d4numFields(data(aDbfs(i)))
                        X = d4fieldJ(data(aDbfs(i)), j)
                        Select Case aDbfs(i)
                        Case "group"
                            data.Add X, "g_" + f4name(X)
                        Case "relation"
                            data.Add X, "r_" + f4name(X)
                        Case "notes"
                            data.Add X, "n_" + f4name(X)
                        Case "history"
                            data.Add X, "h_" + f4name(X)
                        Case "e_mail"
                            data.Add X, "e_" + f4name(X)
                        Case Else
                            data.Add X, f4name(X)
                        End Select
                    Next j
                End If
            Else
                ' fields
                For j = 1 To d4numFields(data(aDbfs(i)))
                    X = d4fieldJ(data(aDbfs(i)), j)
                    data.Add X, f4name(X)
                Next j
            End If
        Else
            MsgBox "Unable To Open " + aDbfs(i)
        End If
    Next i
Exit Sub

ERR_HANDLER:
    MsgBox ("Error Opening Data")
End Sub

